Welcome.
### Just see how it looks
* [Creating the newsletter video](https://drive.google.com/file/d/0B7SJ2q4GS5n6TFU5UkEzUl9tRk0/view?usp=sharing)
* [Copying the book into new book and fill with data for other language video](https://drive.google.com/file/d/0B7SJ2q4GS5n6UW43bkV0VU5oSlE/view?usp=sharing)
### Target - What to provide ###

* An ability for creating printable newsletters
* Support for collaborative work while creating newsletter
* User Interface with abilities of WYSIWYG
* Using of the articles of the unesco.kz to pre-fill pages of the newsletter: with text and images

### Technology ###

The conception of the thin client was not successful.
Today we can doing heavy work at the Frontend-side without any lags of UI.
The technologies is ready. 

Many languages can be used for any taste.
Many frameworks. Many code.

**[React](https://reactjs.org/)** was selected because it was actively and successfully developed by Facebook and development continues in proper way.
The entry threshold is quite low, and the opportunities provided are very high.

The full stack of used technologies:

* [React](https://reactjs.org/) 16 with Fiber 
* [Material UI](http://www.material-ui.com) components
* [Redux](http://redux.js.org/) as a predictable state container
* Small PHP script for support a storing data at backend
* Small PHP script for retrieving data from UNESCO Almaty website

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

**PhpStorm** has good support of React and was selected because historically used for php last year.
Ability of using one IDE for Frontend and Backend it is just faster development.
Also, JetBrains platform was previously used by me for Pure Java and for Android.

### Build & Run ###

1. [Download Node](https://nodejs.org/en/) *I used version 6.11.2 LTS*
2. ```npm install -g create-react-app```
3. Run [PhpStorm](https://www.jetbrains.com/phpstorm/)
4. Clone project
5. Open project
6. Open package.json and select *Run npm-install*/*Run npm-update*
7. In menu *Tools* select *npm task* (Alt+F11 and select npm, Or open npm panel at left down angle)
8. Select *start* to Run at local node server
9. Select *build* for create files ready for uploading to remote server

![Screen Shot 2017-08-05 at 6.27.24 PM.png](https://bitbucket.org/repo/ak5Ly6k/images/1361718380-Screen%20Shot%202017-08-05%20at%206.27.24%20PM.png)

[LICENSE](https://bitbucket.org/ay_from_almaty/unesco.kz-newsletter-generator/raw/95c23963371bf1a09e357e6804c7c2a04429ed65/LICENSE)

Thank you