// @flow weak

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Snackbar from 'material-ui/Snackbar';
import { connect } from 'react-redux'

const styles = theme => ({});

class Status extends Component {
    componentWillReceiveProps(nextProps) {
        if(nextProps.error) this.tick(nextProps.error.willResolveAt)
    }
    tick(end) {
        this.setState({
            timer: Math.round((end - Date.now()) / 1000)
        })
    }
    componentDidUpdate() {
        if(this.props.error && this.state.timer>0 && this.props.error.willResolveAt)
            setTimeout(this.tick.bind(this),1000,this.props.error.willResolveAt)
    }
    render() {
        //console.log("render Status")
        const {error} = this.props
        const message = error !== undefined
            ? error.message + (this.props.error.willResolveAt?this.state.timer+". sec to retry":"")
            : ""
        return <div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={error !== undefined}
                SnackbarContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{message}</span>}
            />
        </div>
    }
}

Status.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default connect(
    state => ({error: state.backend.error}),
    dispatch => ({})
)(withStyles(styles)(Status))