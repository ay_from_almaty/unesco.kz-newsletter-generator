import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PageField extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if(this.props.value!==nextProps.value) {
            if(!this.editable) return true // something wrong, re-render()
            //console.log("update field "+this.props.name+" in shouldComponentUpdate for "+this.editable)
            this.editable.innerHTML = nextProps.value?nextProps.value:""
        }
        return false
    }
    render() {
        return <div id={this.props.name}>
            <div ref={(editable) => {
                if(!editable) return
                this.editable = editable
                editable.innerHTML=this.props.value?this.props.value:""
            }}
              contentEditable={true}
              suppressContentEditableWarning={true}
              placeholder="[Enter text here]"
              onFocus={this.onEditBegin.bind(this)}
              onBlur={this.onEditEnd.bind(this)}
              onPaste={this.onPaste.bind(this)}
              onKeyDown={this.onKeyDown.bind(this)}></div>
        </div>
    }
    onEditBegin(event) {
        if(this.props.name==="title" && !event.target.innerHTML) {
            // litle hack for title field: jump to second line at start
            setTimeout(()=>{
                document.execCommand("insertHTML", undefined, "<br><br>")
            },500)
        }
        event.target.parentElement.classList.add("edit")
        this.props.onEditBegin(this.props.name,event.target.innerHTML)
        window.addEventListener("beforeunload", this.onBeforeUnload)
    }
    onEditEnd(event) {
        event.target.parentElement.classList.remove("edit")
        window.removeEventListener("beforeunload",this.onBeforeUnload)
        this.props.onEditEnd(this.props.name, event.target.innerHTML)
    }
    onPaste(event) {
        setTimeout(()=>{
            document.execCommand("selectAll")
            document.execCommand("removeFormat")
            document.execCommand("font",undefined,"Arial")
        },500)
    }
    onKeyDown(event) {
        let allow = event.key === "Escape" // maybe we need here do undo all changes
        if(!allow) allow = (event.ctrlKey || event.metaKey) && event.key==="s"

        if(!allow) return // event not handled

        event.target.blur()
        event.preventDefault()
    }
    onBeforeUnload = (e) => {
        this.props.onEditEnd(this.props.name)
        // listener will be deleted
    }

}

PageField.propTypes = {
    name: PropTypes.string.isRequired,
}
