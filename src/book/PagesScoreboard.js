import React, { Component } from 'react';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import './PagesScoreboard.css'
import constants from '../constants'

const styles = theme => ({
    progress: {
        margin: 0
    },
    button: {
        margin: theme.spacing.unit,
    },
    button_help: {
        margin: theme.spacing.unit,
        pointerEvents: "auto",
    },
});

class PageScoreboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sectionIndex: 0,    // scrolled to section with index
            page: 0,             // scrolled to page
            showShare: true
        }
        this.globalPage = 0
        this.scrollHandler = (e) => { this.onScroll(window.scrollY) }
        this.preparePages(this.props.bookLayout)
    }
    preparePages(bookLayout) {
        if(!bookLayout.length) {
            this.pages = undefined // drop pages
            return
        } // avoid reduce on empty array
        this.pages = bookLayout.map((sectionLayout, sectionIndex) => {
            return new Array(Number(sectionLayout.pagesCount))
                .fill(undefined)
                .map((nothing,index) =>
                {return {
                    sectionIndex: sectionIndex,
                    pageIndex: index
                }})
        })
        this.globalPages = this.pages.reduce((globalPages, localPages)=>globalPages.concat(localPages),[])
    }
    componentWillReceiveProps(nextProps) {
        // bookLayout changed - update this.pages & this.globalPages
        if(this.props.bookLayout !== nextProps.bookLayout) this.preparePages(nextProps.bookLayout)
        if(nextProps.backendMessage!==undefined) this.setState({showShare: false})
    }
    componentDidMount() {
        window.addEventListener('scroll', this.scrollHandler)
    }
    pageHeight() {
        if(this._pageHeight === undefined) {
            const pages = document.getElementsByClassName("page")
            if (!pages || !pages.length) return 0
            this._pageHeight = pages[0].offsetHeight
        }
        return this._pageHeight
    }
    componentWillUnmount() {
        window.removeEventListener('scroll',this.scrollHandler)
    }
    scrollTo(section, page) {
        if(this.props.page!==undefined                  // disable while editing fields
            || (this.props.previewData!==undefined
                && this.props.previewData.id!==undefined)    // disable while preview
        ) return
        if(!this.props.bookLayout || !this.props.bookLayout.length) return // avoid reduce on empty array

        var scrollY = 0
        const pageHeight = this.pageHeight()
        this.pages.every((pageAnchor)=>{
            const sectionLayout = this.props.bookLayout[pageAnchor[0].sectionIndex]
            if(sectionLayout.section === section) {
                if(page !== undefined) scrollY += page*(pageHeight+18)
                return false
            }
            scrollY += (pageHeight+18)*Number(sectionLayout.pagesCount) // include vertical space between pages
            return true
        },0)

        window.scrollTo(window.scrollX, scrollY)
        //this.onScroll(scrollY)
    }
    onScroll(scrollY) {
        if(this.props.page!==undefined                  // disable while editing fields
            || (this.props.previewData!==undefined
                && this.props.previewData.id!==undefined)    // disable while preview
        ) return

        // +0.4 to commit only page with visible area bigger than visible area of previous page
        const globalPageIndex = Math.trunc(scrollY/(this.pageHeight())+0.4)

        // calculate only if last visible page changed
        if(globalPageIndex !== this.globalPage && globalPageIndex<this.globalPages.length) {
            const page = this.globalPages[globalPageIndex]
            const sectionIndex = page.sectionIndex

            const state = {page: page.pageIndex}

            if(sectionIndex !== this.state.sectionIndex) {
                this.setState({ ...state, sectionIndex: sectionIndex })
            } else {
                this.setState(state)
            }

            this.globalPage = globalPageIndex

            if(this.props.onScroll)
                if(this.props.bookLayout[sectionIndex])
                    this.props.onScroll(this.props.bookLayout[sectionIndex].section, page.pageIndex, sectionIndex)
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.bookLayout.length===0) return

        const editOrPreview = (preview,page) => (preview && preview.id) || page!==undefined // editing or preview

        if(editOrPreview(prevProps.previewData, prevProps.page)
            && !editOrPreview(this.props.previewData,this.props.page)) { // catch changing from editing to non-editing
            if(window.scrollY===0) { // scroll position not changed at return from "single page" mode
                setTimeout(this.scrollTo.bind(this),
                    0, this.props.bookLayout[this.state.sectionIndex].section,this.state.page)
            }
            return
        }
        if(prevProps.backendMessage!==undefined
            && this.props.backendMessage===undefined) setTimeout(
                ()=>this.props.backendMessage===undefined && this.setState({showShare: true}), // enable share button
            1610)                                                                              // after 1610 ms
    }
    render() {
        const isPreview = this.props.previewData && this.props.previewData.id
        return <div id="pagesScoreboard"
                    className={
                        (this.state.sectionIndex%2?"even":"odd")+(this.props.page!==undefined||isPreview?" page":"")
                    }>
            { this.props.bookLayout.length>0 ? this.renderDefault() : this.renderHelp() }
            { this.props.bookLayout.length>0 && (this.props.page!==undefined
                ? this.renderEdit()
                : isPreview ?this.renderPreview():null)}

            { this.renderProgress(this.props.backendMessage!==undefined) }
            {!isPreview && this.props.page===undefined && this.state.showShare
            && <div className="inner" style={{pointerEvents:"none"}}><div style={{
                width: "100%",
                textAlign: "right",
                marginRight: "10px",
            }}><div style={{marginLeft:0}} className="help">
                <Button raised className={this.props.classes.button_help} onClick={()=>this.props.showHelp("Share")}>
                    Share position
                </Button>
            </div></div></div>}
        </div>
    }

    renderHelp = () => <div className="inner">
            <div>&lt;&lt; {!this.props.book
                ? "Please, create the book"
                : "Please, create the page in section"} on SETTINGS tab
            </div>
        </div>

    renderDefault = () => {
        const section = this.props.bookLayout[this.state.sectionIndex].section
        return <div id="managePages" className="inner">
        <div>
            <select
                disabled={!this.props.bookLayout.length>0}
                value={section}
                onChange={ (event) => this.scrollTo(event.target.value) }
                style={{width:200,float: "left"}}>
                { this.props.bookLayout.map((sectionLayout) =>
                    <option key={sectionLayout.section}>{sectionLayout.section}</option>) }
            </select>
        </div>
        <div id="navPages">
            {
                this.pages
                    ? this.pages[this.state.sectionIndex].map((pageAnchor,index) =>
                        <span
                            key={index}
                            onClick={()=>
                                this.scrollTo(
                                    this.props.bookLayout[pageAnchor.sectionIndex].section,
                                    index)}
                            className={
                                (this.state.sectionIndex%2?"even":"odd")
                                +(index===this.state.page?" selected":"")}>{index+1}</span>)
                    :""
            }
        </div>
    </div>}

    renderEdit = () => <div id="managePage" className="inner">
        <div>
            <b>{ this.props.section }/{ this.props.page+1 }/{this.props.field}</b>.
            <span style={{paddingLeft:15}}><b>Esc</b> or <b>Ctrl-S</b> to end the editing</span>
        </div>
    </div>

    renderPreview = () => <div id="previewPage" className="inner">
        {this.props.bookLayout.length>0 && <div>
            <Button raised className={this.props.classes.button} onClick={this.onSavePreview}>
                Save as page #{this.props.previewData.page+1} in {this.props.previewData.section}
            </Button>
        </div> }
        <div style={{marginLeft:0}}>
            <Button raised className={this.props.classes.button} onClick={()=>this.props.cancel(this.props.previewData)}>
                Cancel
            </Button>
        </div>
        <div style={{marginLeft:0}} className="help">
            <Button raised className={this.props.classes.button_help} onClick={this.props.showHelpPreview}>
                Help
            </Button>
        </div>
    </div>

    renderProgress = (show) => {
        this.lastMessage = this.props.backendMessage ? this.props.backendMessage : this.lastMessage
        return <div id="backend" className={"inner"+(show?" show":"")} >
            <div>{this.lastMessage}</div>
            <div id="progress">
                 <CircularProgress className={this.props.classes.progress} size={35}/>
            </div>
        </div>
    }

    onSavePreview = () => {
        this.props.save(this.props.previewData)           // just local in-memory save
        if(this.props.onPreviewSaved) this.props.onPreviewSaved(this.props.previewData) // delegate to save to backend
    }

    lastMessage = ""
}

PageScoreboard.propTypes = {
    classes: PropTypes.object.isRequired,
    onPreviewSaved: PropTypes.func.isRequired,
    onScroll: PropTypes.func.isRequired,
    bookLayout: PropTypes.array.isRequired
}

export default connect(
    state => ({
        backendMessage: state.backend.message,
        previewData: state.previewData,
        book: state.booksList.list[state.booksList.selectedBook]
    }),  // share all book, we will use keys props.section/props.page
    dispatch => ({
        save: preview => { // in-memory save
            dispatch({
                type: "bookData."+preview.section+"."+preview.page,
                payload: Object.entries(preview)
                    .filter(([field])=>constants.pageLayout.includes(field)) // ONLY fields included in Page Layout
                    .reduce((acc,[field,value])=>({...acc,[field]: value}),{})
            })
            dispatch({
                type: "previewData",
                payload: Object.keys(preview)
                    .reduce((acc,field)=>({...acc,[field]: undefined}),{})
            })
        },
        cancel: (preview) => {
            dispatch({
                type: "previewData",
                payload: Object.keys(preview)
                    .reduce((acc,field)=>({...acc,[field]: undefined}),{})
            })
        },
        showHelp: (type) => dispatch({ type: "help",payload: type?type:"Preview" })
    })
)(withStyles(styles)(PageScoreboard))