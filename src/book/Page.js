import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './Page.css';
import PageField from './PageField';
import Header from './Header';
import { connect } from 'react-redux';

class Page extends Component {
    state = {
        locked: false,
    }
    shouldComponentUpdate(nextProps, nextState) {
        if(this.state!==nextState) return true
        if(nextProps.previewData!==this.props.previewData           // preview data changed
            && nextProps.previewData.section === this.props.section  // section in preview same as section in input
            && nextProps.previewData.page === this.props.page) {     // page in preview same as page in input
            return true
        }
        if(!nextProps.bookData) return false
        if(!nextProps.bookData[nextProps.section]) return false
        if(nextProps.bookData[nextProps.section][this.props.page]!==this.data) {
            return true
        }
        if(nextProps.className !== this.props.className) return true
        return false
    }
    data() {
        const preview = this.props.previewData
        && this.props.previewData.section === this.props.section
        && this.props.previewData.page === this.props.page
            ? this.props.previewData
            : undefined
        const bookData = this.props.bookData && this.props.bookData[this.props.section]
            ? this.props.bookData[this.props.section][this.props.page]
            : undefined
        return preview?preview:bookData
    }
    render() {
        const _data = this.data()
        const locked = _data?this.state.locked:true // lock page if no data (downloading)
        const data = _data?_data:{}

        return <div size="A4"
                    className={ "page " + this.props.className }
                    style={!this.props.onEditBegin?{pointerEvents:"none"}:{}}>
            <div id="shade"
                 className={ locked?"on":"" }>
            </div>
            <Header name="header" value={data.header}
                    onChanges={ value=>this.props.onEditEnd(this.props.section,this.props.page,"header",value) }/>
            <div id="top_header"></div>
            <div id="section_title_outer_rotated">
                <div id="section_title_inner">{this.props.section}</div></div>
                <PageField name="title" value={data.title}
                           onEditBegin={this.onEditBegin.bind(this)} onEditEnd={ this.onEditEnd.bind(this) }/>
                <PageField name="content" value={data.content}
                           onEditBegin={this.onEditBegin.bind(this)} onEditEnd={ this.onEditEnd.bind(this) }/>
                <PageField name="footer" value={data.footer}
                           onEditBegin={this.onEditBegin.bind(this)} onEditEnd={ this.onEditEnd.bind(this) }/>
        </div>
    }
    // route events from contentEditable fields
    onEditBegin(name,value) {
        this.setState({locked:true});
        this.props.onEditBegin(this.props.section,this.props.page,name,value)
    }
    onEditEnd(name,value) {
        if(this.props.previewData && this.props.previewData.section === this.props.section
            && this.props.previewData.page === this.props.page) { // work with preview
            this.setState({locked:false})
            this.props.onEditEnd(this.props.section,this.props.page,name,undefined) // old non-edited value
            this.props.editPreview(name,value)
        } else {
            this.setState({locked:false});
            this.props.onEditEnd(this.props.section,this.props.page,name,value)
        }
    }
}

Page.propTypes = {
    section: PropTypes.string.isRequired,
    page: PropTypes.number.isRequired,
    onEditBegin: PropTypes.func,
    onEditEnd: PropTypes.func,
}

export default connect(
    state => ({
        bookData: state.bookData,
        previewData: state.previewData // one page preview
    }),  // share all book, we will use keys props.section/props.page
    dispatch => ({
        editPreview: (name,value) => dispatch({
            type: "previewData",
            payload: {[name]: value}
        })
    }) // onEditEnd sends changes
)(Page)