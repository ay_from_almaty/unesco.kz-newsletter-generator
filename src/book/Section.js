import React, { Component } from 'react';
import Page from './Page.js';
import {connect} from 'react-redux'

class Section extends Component {
    constructor(props) {
        super(props)
        this.pages = new Array(this.props.pagesCount).fill(undefined)
    }
    componentWillReceiveProps(nextProps) {
        if(this.props.pagesCount !== nextProps.pagesCount)
            this.pages = new Array(nextProps.pagesCount).fill(undefined)
    }
    render() {
        // some page selected for editing, hide other pages
        const [_section,_page] = this.props.previewData && this.props.previewData.section!==undefined
            ?[this.props.previewData.section, this.props.previewData.page]:[this.props.section,this.props.page]
        const isHidden = _page===undefined
            ?page => false
            // other section or other page in selected section
            :page => this.props.name!==_section||_page!==page
        return <div>
            { this.pages.map((nothing,page) =>
                <Page key={page}
                      page={page}
                      section={this.props.name}
                      className={
                          this.props.className
                          +" first" // first page in section, 01.10.17 temporary solution
                          +(isHidden(page)?" hide":"")
                      }
                      onEditBegin={this.props.onEditBegin}
                      onEditEnd={this.props.onEditEnd}
                />) }
        </div>
    }
}

export default connect(
    state => ({
        previewData: state.previewData,
    }),
    dispatch => ({})
)(Section)