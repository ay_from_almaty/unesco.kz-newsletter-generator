import React, { Component } from 'react';

export default class Header extends Component {
    state =  {
        message: undefined,
        className: undefined,
        progress: 0
    }
    render() {
        return <div id="header"
                    style={this.props.value?{backgroundImage: "url("+this.props.value+")"}:{}}
                    onDrop={this.onDrop.bind(this)}
                    onDragLeave={this.onDragLeave.bind(this)}
                    onDragEnter={this.onDragEnter.bind(this)}
                    onDragOver={this.onDragOver.bind(this)}
                    onClick={this.onDragLeave.bind(this)}
                    className={this.state.className}>
            <div className="inner"
                 style={this.props.value||this.state.message?{opacity:0}:{opacity:1}}>
                <div id="help">
                    [Drag&nbsp;image&nbsp;here]<br/>
                    Proportions is <b>16:9</b> (210mm : 118.125mm)
                    {/*or*/}
                    {/*<br/> [Drag link to unesco.kz article here]*/}
                    {/*<br/><br/>When you drop the link to unesco.kz here then all fields of the page goes filled,*/}
                    {/*you can replace content later manually.*/}
                </div>
            </div>
            <div className="inner"
                 style={this.state.message?{opacity:1}:{opacity:0}}>
                <div id="help">
                    {this.state.message}
                </div>
            </div>
        </div>
    }

    onDragEnter(e) { // determine that we can handle this or not
        e.preventDefault()
        e.stopPropagation()

        if(e.dataTransfer.types.find(type =>
            {switch(type) {
                case "text/uri-list":this.dropPromise = this.onDropURL;return true
                case "Files":
                    if(e.dataTransfer.items===undefined) return true;// not supported by browser
                    for(let i=0;i<e.dataTransfer.items.length;i++) {
                        if(e.dataTransfer.items[i].type.startsWith("image/")) {
                            this.dropPromise = this.onDropImage; return true
                        } // one file of files is image
                    }
                    return false
                default: return false
            }}
        )) {
            this.setState({
                message: "Good. We probably can handle this, drop here and see what happened.",
                className: "good"
            })
        } else {
            this.dropPromise = undefined
            this.setState({
                message: "I'm Sorry, this action will do nothing." +
                         " Just release mouse button over black area to cancel action.",
                className: "error"
            })
        }
    }

    onDragOver(e) {
        e.preventDefault()
        e.stopPropagation()
    }

    onDragLeave(e) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({message: undefined, className: undefined})
    }

    onDrop(e) {
        e.preventDefault()
        e.stopPropagation()
        if(this.dropPromise===undefined) return // we know already that we can't handle this drop

        this.dropPromise(e).then(
            ()=>this.setState({message: undefined,className: undefined}),
            error=>{
                this.setState({message:error.message, className:"error"})
                setTimeout(()=>this.setState({message: undefined, className: undefined}), 10000)// hide error after 10s
            }
        )
        this.dropPromise = undefined // no needed anymore
    }

    // drop hanslers
    dropPromise = undefined // become filled in onDragEnter and become used in onDrop
    async onDropURL(event) {
        const url = event.dataTransfer.getData("text/uri-list")
        var i = url.indexOf("unesco.kz/")

        if(i > -1 && url.indexOf("/",i+10) === -1) {
            console.log("FIXME: realize fetching for "+url)
            return
        }

        throw Error("Only urls like '...unesco.kz/some-readable-string'" +
            " can be used for fetching the data, '"+url+"' is wrong url")
    }
    async onDropImage(event) {
        for(let i=0;i<event.dataTransfer.files.length;i++) {
            const file = event.dataTransfer.files[i]
            if(file.type.startsWith("image/")) {
                const content = await new Promise((resolve, reject) => { // wrapper for "synchronous" processing
                    let reader = new FileReader()
                    reader.addEventListener("loadend", ()=>{
                        if(reader.error) reject(new Error(reader.error.name))
                        else if(reader.result) resolve(reader.result)
                        else reject(new Error("There is no data was read from dragged file"))
                    })
                    reader.readAsDataURL(file)
                })
                this.props.onChanges(content)
                return
            }
        }
    }
}