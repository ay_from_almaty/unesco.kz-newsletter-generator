import React, { Component } from 'react';
import Section from './Section.js';
import PagesScoreboard from './PagesScoreboard.js';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class Book extends Component {
    constructor(props) {
        super(props)
        this.state = {
            section: undefined,     // section of currently edited page
            page: undefined,        // index of currently edited page
            field: undefined        // field on currently edited page
        }
    }
    render() {
        // console.group("book")
        // console.log(this.props)
        // console.log(this.state)
        // console.groupEnd()
        let i = 0
        const bookLayout = Object.entries(this.props.sections)
            .filter(([n,sectionLayout])=>sectionLayout) // filter undefined, any case
            .reduce((acc,[n,sectionLayout])=>[...acc,sectionLayout],[])
        return <div>
            <div id="pages">
                {bookLayout.map((sectionLayout) =>
                    <Section key={sectionLayout.section}
                             name={sectionLayout.section}
                             pagesCount={Number(sectionLayout.pagesCount)}
                             className={
                                 (i++ % 2 ? "even" : "odd")
                                 + (this.props.showBorders?" showBorders":"")
                                 + (sectionLayout.hideHeader?" hide_header":"")
                             }
                             section={ this.state.section }
                             page={ this.state.page }
                             onEditBegin={ this.onEditBegin.bind(this) }
                             onEditEnd={ this.onEditEnd.bind(this) }
                    />)}
            </div>
            <PagesScoreboard
                bookLayout={ bookLayout }
                onScroll={ this.props.onScroll }
                section={this.state.section}
                page={this.state.page}
                field={this.state.field}
                onPreviewSaved={this.props.onPreviewSaved}
            />
        </div>
    }
    // route events from contentEditable fields
    onEditBegin(section,page,field,value) {
        if(this.props.onEditBegin) this.props.onEditBegin(section,page,field,value)
        this.setState({section:section,page:page,field:field})
    }
    onEditEnd(section,page,field,value) {
        if (this.props.onEditEnd) this.props.onEditEnd(section,page,field,value)
        this.setState({section: undefined, page: undefined,field:undefined})
    }
}

Book.propTypes = {
    sections: PropTypes.object.isRequired,
    onPreviewSaved: PropTypes.func.isRequired,
    onScroll: PropTypes.func.isRequired,
    onEditBegin: PropTypes.func.isRequired,
    onEditEnd: PropTypes.func.isRequired,
}

export default connect(
    state => ({
        showBorders: state.mainBoard.pages.Settings?state.mainBoard.pages.Settings.showBorders:false,
        sections: state.bookLayout.sections,
    }),
    dispatch => ({})
)(Book)