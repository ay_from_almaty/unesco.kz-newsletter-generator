import React from 'react';

export default (props) => <div>
    <p>You see preview of the page that filled by data from the article from unesco.kz web-site</p>
    <ul>
        <li>In the top of the page you have two buttons to end the previewing</li>
        <li>You can save the data or just cancel</li>
        <li>You can edit the fields before save the page</li>
        <li>To save changes to page, click the button SAVE.<br/>
            <b>Data will be saved on the server and will be accessible by other</b></li>
        <li>The data will be saved in existing page (the section and the page see on the SAVE button).</li>
        <li><b>Don't worry, the Newsletter will NOT be modified until you especially click the SAVE button.</b></li>
    </ul>
</div>