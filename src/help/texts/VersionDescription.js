import React from 'react';
import constants from '../../constants'

// you should update changelist manually
export default (props) => <div>
    <p>The new version "{constants.version}" installed.</p>
    <ul>
        <li>ability for hiding the header of the pages from desired section</li>
        <li>ability for sharing the position of the book</li>
        <li>fixed unexpected behaviour in the sections management</li>
        <li>fixed unexpected behaviour in the books management</li>
    </ul>
</div>
