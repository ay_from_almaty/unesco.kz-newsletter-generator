import React from 'react';

export default (props) => <div>
    <p>Here you can extract the data from database of unesco.kz.</p>
    <ul>
        <li>These articles is the same as displayed on website.</li>
        <li>Results of searching is a list of the articles grouped by month.</li>
        <li>Click on month to collapse/expand the group.<br/>
            Click EXPAND ALL/COLLAPSE ALL to apply action to all displayed groups</li>
        <li>If you want to view the article that represented by some card, just click on it.<br/>
            <b>Don't worry, the Newsletter will NOT be modified until you especially click the SAVE button.</b></li>
        <li>In preview mode click the HELP button to see additional help.</li>
        <li>To extract articles for particular themes only, click ADD FILTER<br/>
            - feel free to click strings in the list in the opened window, <i>ESC</i> closes window</li>
        <li>To extract articles for another language, click ADD FILTER and select desired language</li>
        <li>You can do final filtering by provide some text in the field SEARCH.</li>
    </ul>
</div>