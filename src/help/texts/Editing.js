import React from 'react';

export default (props) => <div>
    <p>For the editing of the fields used standard 'WYSIWIG' editor which is like Word</p>
    <ul>
        <li>In 80% cases you will use basic features</li>
        <li>Use Shift+Enter instead of Enter to precise control of free space.</li>
        <li>You can try something and use Ctrl-Z to undo, and Ctrl-Y</li>
        <li>Hot keys Ctrl-B, Ctrl-I and etc.</li>
        <li>See hotkey in Word and try here</li>
        <li><b>Esc</b> or <b>Ctrl-S</b> to end the editing</li>
        <li style={props.editPageField==="content"?{}:{textDecoration: 'line-through'}}>
            Try to drag something, like image from another page
        </li>
    </ul>
</div>