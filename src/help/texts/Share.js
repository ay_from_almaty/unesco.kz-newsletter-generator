import React from 'react';


export default (props) => <div>
    You can find URL copied into Clipboard.<br/>
    Paste (<i>Ctrl+V</i>) it into Email/Skype/WhatsApp/etc message.<br/>
    The recipient can open the book at same position.
    <input style={{width:1,opacity:0}} ref={(input)=>{
        if(!input) return
        input.select()
        document.execCommand("Copy")
    }} type="text" defaultValue={document.location+"?localStorage="+encodeURI(JSON.stringify({
        scroll:window.scrollY,
        mainBoard:JSON.parse(localStorage.getItem("mainBoard")),
        "booksList.selectedBook":localStorage.getItem("booksList.selectedBook"),
    }))}/>
</div>