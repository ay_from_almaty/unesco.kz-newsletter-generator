import React from 'react';
import MoreVertIcon from 'material-ui-icons/MoreVert';
import DeleteIcon from 'material-ui-icons/Delete';
import UpIcon from 'material-ui-icons/ArrowUpward';
import DownIcon from 'material-ui-icons/ArrowDownward';
import SettingsIcon from 'material-ui-icons/Settings';

export default (props) => <div>
    <p>Here you can manage list of the books:</p>
    <ul>
        <li>Select the book for working on it</li>
        <li>Create new book (you can copy existing book too)</li>
        <li>Rename the book</li>
        <li>Delete the book</li>
    </ul>
    <p>After book creation, You can define layout of the book with the following operations:</p>
    <ul>
        <li>Add new section</li>
        <li>Edit section name</li>
        <li>Change count of the pages in the particular section</li>
    </ul>
    <p>Click the <MoreVertIcon/> at right side of the sections list to open menu with the following operations:</p>
    <ul>
        <li>Move the section up  <UpIcon/></li>
        <li>Move the section down  <DownIcon/></li>
        <li>Additional settings for the section, like "Hide header" <SettingsIcon/></li>
        <li>Delete the section  <DeleteIcon/></li>
    </ul>
</div>