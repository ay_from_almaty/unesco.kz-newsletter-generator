// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Snackbar from 'material-ui/Snackbar';
import { connect } from 'react-redux'
import Preview from './texts/Preview'
import Layout from './texts/Layout'
import Share from './texts/Share'
import VersionDescription from './texts/VersionDescription'
import DataProvider from './texts/DataProvider'

const styles = theme => ({});

/** help component by name */
const help = help => {
    switch(help.name?help.name:help) { // help can store name or object {name: ..., ....}
        case "Preview": return <Preview {...help.payload}/> // payload used as props
        case "Layout": return <Layout {...help.payload}/>
        case "Share": return <Share {...help.payload}/>
        case "DataProvider": return <DataProvider {...help.payload}/>
        case "VersionDescription": return <VersionDescription {...help.payload}/>
        default: return null
    }
}
/** Display help as Snackbar. Monitors props.help and displays help(props.help)  */
const HelpSnackbar = (props) => {
    if(!this.wasHelp && props.help) { // appeared
        window.addEventListener("keydown",props.clear)
    } else if(this.wasHelp && !props.help) { // disappear
        window.removeEventListener("keydown",props.clear)
    }
    this.wasHelp = props.help

    return <div>
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            open={props.help !== undefined}
            SnackbarContentProps={{
                'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.help=props.help
                ? help(props.help)
                : this.help}</span>} //this.help == last help
            onRequestClose={props.clear}
            onClick={props.clear}
        />
    </div>
}

HelpSnackbar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default connect(
    state => ({help: state.help}),
    dispatch => ({
        clear: () => dispatch({
            type: "help"
        })
    })
)(withStyles(styles)(HelpSnackbar))