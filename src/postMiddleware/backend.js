/** Handler of backend's changes after reducer **/
export default (store, action, app) => {
    const [root, type] = action.type.split('.')
    const state = store.getState()[root]
    if(type==="tasks") { // only if tasks was modified
        const tasksWithoutBook = Object.entries(state.tasks).filter(([,task])=>task!==undefined && task.book===undefined)
        if(tasksWithoutBook.length>0) { // add book to tasks
            //console.log(tasksWithoutBook)
            const book = store.getState().booksList.list[store.getState().booksList.selectedBook]
            store.dispatch({
                type: "backend.tasks",
                payload: tasksWithoutBook.reduce((acc,[n,task])=>({
                    ...acc,
                    [n]: {
                        ...task,
                        book: book && Object.keys(task).find(name=>name.indexOf("booksList.")===-1)
                            ? book.name
                            : ""} // using '' for booksList.*
                }),{})
            })
            return
        }
        //console.log(action)
        let downloadTasks = 0, uploadTasks=0
        let tasks = {} // where task!==undefined in {index: task}
        const save = { // save only 'upload' tasks, where value!==undefined in {index: {name: value}}
            tasks: Object.entries(state.tasks).reduce((acc, [n, task]) => {   // filter download tasks
                if (!task) return acc// filter empty task
                tasks = {...tasks, [n]: task};
                const value = Object.entries(task)[0][1]
                if(value===null) downloadTasks++
                else uploadTasks++
                return value !== undefined && value !== null ? {...acc, [n]: task} : acc
            }, {}),
        }
        localStorage.setItem(root, JSON.stringify(save))
        if (downloadTasks+uploadTasks) {
            if(!app.loading){ store.dispatch({
                type: 'backend.message',
                payload: "\u2191"+uploadTasks+" \u2193"+downloadTasks+" changes"})
                app.resolveTasks(tasks)}
        } else { // no tasks, probably the first batch of data loaded
            if(!app.scrollInitiated) {
                setTimeout(()=>{
                    const scrollY = localStorage.getItem("scroll")
                    if(scrollY) window.scrollTo(window.scrollX, scrollY)
                },300,this)
                app.scrollInitiated = true
            }
        }
    }
}
