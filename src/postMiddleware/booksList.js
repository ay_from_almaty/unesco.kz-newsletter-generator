/** Handler of booksList's changes after reducer **/
export default (store, action) => {
    if(action.payload.selectedBook!==undefined) {
        if(action.payload.selectedBook!==null) {          // we almost ready for the loading selected book
            if(store.getState().bookLayout.lastSectionIndex!==null) store.dispatch({
                type:"bookLayout",
                payload:{lastSectionIndex: null}          // to empty layout & data of the previous selected book
            })
            if(action.payload.selectedBook!=="") {
                const index = store.getState().backend.lastTaskIndex + 1// CHAINED LOADING ALL BOOK FROM BACKEND
                store.dispatch({                          // load bookLayout.lastSectionIndex, trigger loading of book
                    type:"backend.tasks."+index,          // NEXT CHAIN LINK preMiddleware/bookLayout
                    payload:{"bookLayout.lastSectionIndex":null}
                })
                store.dispatch({type:"backend.lastTaskIndex", payload: index})
            }
        }
    }
}
