/** Handler of mainBoard's changes after reducer **/
export default (store, action) => {
    const [root, type, subType] = action.type.split('.')
    const state = store.getState()[root]
    if(subType==="Help"
        || (state.pages.Help && state.pages.Help.editPageField)
        || type==="className"
        || action.payload.className) {
        return // skip saving
    }
    localStorage.setItem(root, JSON.stringify({...state, className: undefined })) // exclude "className"
}
