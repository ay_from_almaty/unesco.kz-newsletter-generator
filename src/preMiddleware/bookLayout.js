import constants from '../constants'

/** Handler of bookLayout's changes before it going to reducer **/
export default (store, action) => {
    const [root, type, subType] = action.type.split('.')
    const state = store.getState()[root]
    if(!type) {      // only root
        if(action.payload.lastSectionIndex !== undefined) {  // work with 'lastSectionIndex'
            if(state.lastSectionIndex === null) {     // prev index was empty, let's loading the layout from backend
                if (action.payload.lastSectionIndex === "") return true // skip, no sections
                let taskIndex = store.getState().backend.lastTaskIndex
                store.dispatch({
                    type: 'backend.tasks',
                    payload: {
                        ...new Array(Number(action.payload.lastSectionIndex) + 1)
                            .fill(undefined).reduce((acc, nothing, index) => {
                                return {
                                    ...acc,
                                    ...constants.sectionLayout.reduce((acc2,field)=>({
                                        ...acc2,
                                        [++taskIndex]: {["bookLayout.sections." + index + "." + field]: null},
                                    }),{})
                                }
                            }, {})
                    }
                })
                store.dispatch({type: "backend.lastTaskIndex", payload: taskIndex})
            } else { // regular updates of lastSectionIndex
                if(action.payload.lastSectionIndex===null) { // BOOK CHANGED, this is middle state
                    store.dispatch({                        // empty layout
                        type: "bookLayout.sections",
                        payload: Object.keys(state.sections).reduce((acc,index)=>({
                            ...acc,
                            [index]: undefined  // mark section with 'undefined'
                        }),{})
                    })

                    if(store.getState().bookData) store.dispatch({                        // empty data
                        type: "bookData",
                        payload: Object.keys(store.getState().bookData).reduce((acc,section)=>({
                            ...acc,
                            [section]: undefined // mark section with 'undefined'
                        }),{})
                    })
                    return
                }
                const lastSectionIndex = Number(action.payload.lastSectionIndex)
                if(state.sections[lastSectionIndex+1]) { // delete sections after lastSectionIndex
                    store.dispatch({
                        type: "bookLayout.sections",
                        payload: Object.keys(state.sections).reduce((acc,index)=>({
                            ...acc,
                            ...(index>lastSectionIndex  // has index > lastSectionIndex
                                ? { [index]: undefined } // mark section with 'undefined'
                                : {})
                        }),{})
                    })
                }
            }
        }
    } else if(type==="sections" && subType) {
        const section = action.payload.section?action.payload.section:state.sections[subType].section
        let taskIndex = store.getState().backend.lastTaskIndex
        store.dispatch({    // generating tasks for the loading of bookData for sections
            type: "backend.tasks",
            payload: new Array(Number(action.payload.pagesCount)).fill(1).reduce((acc3,nothing,page)=>({
                ...acc3,
                ...constants.pageLayout.reduce((acc, field)=>({
                    ...acc,
                    [++taskIndex]: {["bookData."+section+"."+page+"."+field]: null}
                }),{})
            }),{})
        })
        store.dispatch({type:"backend.lastTaskIndex", payload: taskIndex})
    }
}