/** Handler of booksList's changes before it going to reducer **/
export default (store, action) => {
    const [root, type] = action.type.split('.')
    const state = store.getState()[root]
    if(!type) {      // only root
        if(action.payload.lastBookIndex !== undefined) {  // work with 'lastBookIndex'
            if(state.lastBookIndex === null) {             // initial loading from backend
                if(action.payload.lastBookIndex!=='' && action.payload.lastBookIndex!==null) {
                    let taskIndex = store.getState().backend.lastTaskIndex
                    store.dispatch({
                        type: 'backend.tasks',
                        payload: {
                            ...new Array(Number(action.payload.lastBookIndex) + 1)
                            .fill(undefined).reduce((acc, nothing, index) => {
                                return {
                                    ...acc,
                                    [++taskIndex]: {["booksList.list." + index + ".name"]: null},
                                }
                            }, {}),
                            [++taskIndex]: {"booksList._selectedBook": null}, // next chain link in postMiddleware
                        }
                    })
                    store.dispatch({type: "backend.lastTaskIndex", payload: taskIndex})
                } else {
                    store.dispatch({
                        type: "booksList",
                        payload: {selectedBook: ""}
                    })
                }
            } else { // regular updates of lastBookIndex
                const lastBookIndex = Number(action.payload.lastBookIndex)
                if(state.list[lastBookIndex+1]) { // delete books after lastBookIndex
                    store.dispatch({
                        type: "booksList.list",
                        payload: Object.keys(state.list).reduce((acc,index)=>({
                            ...acc,
                            ...(index>lastBookIndex  // has index > lastBookIndex
                                ? { [index]: undefined } // mark book with 'undefined'
                                : {})
                        }),{})
                    })
                }
            }
        } else if(action.payload._selectedBook!==undefined) {
                const book = localStorage.getItem("booksList.selectedBook")
                store.dispatch({ // rewrite by local value if exists
                    type: "booksList",
                    payload: {selectedBook: book && state.list[book]!==undefined?book:state.lastBookIndex}
                })
        } else  if(action.payload.selectedBook!==undefined) {
            localStorage.setItem("booksList.selectedBook",action.payload.selectedBook)
        }
    } else if(type==="selectedBook") {
        localStorage.setItem("booksList.selectedBook",action.payload)
    }
}