import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import Book from './book/Book.js'
import MainBoard from './mainBoard/MainBoard.js';
import BackendStatus from './backend/Status'
import './App.css'
import HelpSnackbar from './help/HelpSnackbar'
import constants from './constants'
import bookLayoutPre from './preMiddleware/bookLayout'
import booksListPre from './preMiddleware/booksList'
import booksListPost from './postMiddleware/booksList'
import mainBoardPost from './postMiddleware/mainBoard'
import backendPost from './postMiddleware/backend'

const STORAGE_VERSION = '38'
const endpoint = "http://intranet.unesco.kz/update/newsletter/backend.php"
    + (document.location.toString().indexOf("localhost")!==-1?"?dev=1":"")

const old = localStorage.getItem("VERSION")
const newVersionInstalled = old!==null && old!==STORAGE_VERSION
if(newVersionInstalled) {
    console.log("localStorage: New version '"+STORAGE_VERSION+"', old version '"+old+"': clear storage")
    localStorage.removeItem("tasks")
    localStorage.setItem("VERSION",STORAGE_VERSION)
}

/**
 * Application for create books.
 *
 * Structure:
 * 1. right area of page: things to  work with entire application
 * 2. left area of page: work with book only
 *
 * The book has layout and the data.
 * UI for the book supported by Book/Section/Page* components.
 * These components receives layout of the book in the props.
 * While the data itself comes in Page through Redux (See PageField.js).
 *
 */
export default class App extends Component {
    backendRetryTime = 5000 // ms
    scrollInitiated = false
    constructor(props) {
        super(props)
        const [location,sharedStorage] = document.location.toString().split("?localStorage=")
        if(sharedStorage) {
            Object.entries(JSON.parse(decodeURI(sharedStorage)))           // replace localStorage
                .forEach(([field,value])=>localStorage.setItem(field,JSON.stringify(value)))
            document.location=location
            return
        }
        const initialState = {   // generating an Initial State for store
            mainBoard: this.initialState("mainBoard"),
            backend: this.initialState("backend"),
            bookLayout: this.initialState("bookLayout"),
            booksList: this.initialState("booksList"),
        }
        this.mainBoardStore = createStore(this.reducer,
            initialState,
            window.__REDUX_DEVTOOLS_EXTENSION__ // add middleware, injected by redux-devtools browser's extension
                ? compose(applyMiddleware(this.middleware), window.__REDUX_DEVTOOLS_EXTENSION__())
                : applyMiddleware(this.middleware))
        const index = initialState.backend.lastTaskIndex + 1// start CHAINED INITIAL LOADING FROM BACKEND
        this.mainBoardStore.dispatch({ // triggering the chain. NEXT CHAIN LINK preMiddleware/bookList
            type:"backend.tasks."+index,
            payload:{"booksList.lastBookIndex":null}
        })
        this.mainBoardStore.dispatch({type:"backend.lastTaskIndex", payload: index})
        if(newVersionInstalled) {
            this.mainBoardStore.dispatch({type:"help", payload: "VersionDescription"})
        }
    }
    render() {
        // add "showBorders" to display borders for divs placed on the Page
        return <div><Provider store={this.mainBoardStore}><div>
            <Book
                onScroll={ (section,page,sectionIndex)=>{
                    const className = sectionIndex%2?"even":"odd"
                    if(this.mainBoardStore.getState().mainBoard.className!==className)
                        this.mainBoardStore.dispatch({
                            type: "mainBoard.className",
                            payload: className
                        })
                    this.mainBoardStore.dispatch({
                        type: "scroll",
                        payload: {section: section, page: page, sectionIndex: sectionIndex}
                    })
                    localStorage.setItem("scroll",window.scrollY<1150 ? 0: window.scrollY)
                } }
                onEditBegin={ (section,page,field) => {
                    this.mainBoardStore.dispatch({type:'mainBoard.pages.Help',payload: {editPageField: field}})
                    this._prevTab = this.mainBoardStore.getState().mainBoard.tab
                    this.mainBoardStore.dispatch({type:'mainBoard.tab',payload: 0}) // switch to help tab
                } }
                onEditEnd={ (section,page,field,value)=> { // add new backend task for updating db
                    const data = this.mainBoardStore.getState().bookData[section][page]
                    if(value && data[field]!==value) { // changed?
                        const name = "bookData."+section+"."+page+"."+field
                        const task = {[name]: value}
                        const index = this.mainBoardStore.getState().backend.lastTaskIndex + 1
                        this.mainBoardStore.dispatch({
                            type: 'backend.tasks.'+index,
                            payload: task
                        })
                        this.mainBoardStore.dispatch({ // fix an index of last task
                            type: "backend.lastTaskIndex",
                            payload: index
                        })
                    }
                    if(this._prevTab!==undefined) {
                        this.mainBoardStore.dispatch({type:'mainBoard.tab',payload: this._prevTab})
                        this.mainBoardStore.dispatch({type:'mainBoard.pages.Help',payload: {editPageField: undefined}})
                        this._prevTab = undefined
                    }
                } }
                onPreviewSaved={ preview => {
                    //console.group("onPreviewSaved")
                    //console.log(preview)
                    var index = this.mainBoardStore.getState().backend.lastTaskIndex + 1
                    Object.entries(preview)
                        .filter(([field])=>constants.pageLayout.includes(field)) // ONLY fields included in Page Layout
                        .forEach(([field,value])=>this.mainBoardStore.dispatch({
                            type: 'backend.tasks.'+(index++),
                            payload: {["bookData."+preview.section+"."+preview.page+"."+field]:value}
                        }))
                    this.mainBoardStore.dispatch({ // fix an index of last task
                        type: "backend.lastTaskIndex",
                        payload: index
                    })
                    //console.groupEnd()
                } }


            />
            <MainBoard/><BackendStatus/><HelpSnackbar/></div></Provider>
        </div>
    }
    initialState = (root,state) => { // initial state for concrete root of our state, we have multiple roots
        const loadItem = (name, defaultValue = {}) => { // silently try load item from local storage
            try {
                const data = localStorage.getItem(name)
                const v = typeof(defaultValue) === 'object'?JSON.parse(data):data
                return v?v:defaultValue
            } catch(e) {
                console.error(e)
                return defaultValue
            }
        }
        // here we fill only minimum fields that needed by app
        switch(root) {
            case "mainBoard": return {...loadItem(root, {pages: {}}),className:"odd"}
            case "backend":
                const tasks = loadItem(root, {tasks: {}})
                return {
                    ...tasks,
                    lastTaskIndex: Number(Object.keys(tasks).reduce((acc,index)=>Number(index)>acc?index:acc, -1))
                }
            case "bookLayout": return {sections:{},lastSectionIndex: null}
            case "booksList": return {list:{},lastBookIndex: null}
            default: return {}
        }
    }
    reducer = function(state = {}, action) {   // RAW. MAY PRODUCE KEYS WITH 'UNDEFINED', just handle by self
        if(action.type.startsWith("@@")) return state
        const [root, type, subType] = action.type.split('.')
        const state2 = state[root]?state[root]:{}    // child state, level 2
        const newState = type
            ? subType                     // state[type] is hash?
                ? {
                    ...state2, [type]: {
                        ...state2[type],
                        [subType]: action.payload
                            ? {...state2[type] ? state2[type][subType] : {}, ...action.payload}
                            : undefined
                    }
                }
                : { ...state2, [type]: typeof(action.payload)==='object'
                    ? {...state2[type],...action.payload}
                    : action.payload} // 2 level - primitive type
            :  action.payload && typeof(action.payload)==='object'
                ? {...state2, ...action.payload}
                : action.payload // 1 level - primitive type
        return {...state, [root]: newState}           // 1 level
    }
    preMiddleware = (action) => { // catching an actions did by backend at responses
        const [root] = action.type.split('.')
        if(root==="bookLayout") bookLayoutPre(this.mainBoardStore,action)
        else if(root==="booksList") booksListPre(this.mainBoardStore,action)
    }
    middleware = store => next => action => {
        this.preMiddleware(action)
        const result = next(action)                // to process reducer()
        this.postMiddleware(action)
        return result
    }
    postMiddleware = (action) => { // calls backend only here
        const [root] = action.type.split('.')
        if(root===("mainBoard")) mainBoardPost(this.mainBoardStore, action) // save sub-states of boards as whole object
        else if(root==="backend") backendPost(this.mainBoardStore, action, this)
        else if(root==="booksList") booksListPost(this.mainBoardStore, action)
    }
    resolveTasks(tasks) {
        this.loading = true // block another requests to resolveTasks()
        this.mainBoardStore.dispatch({type: 'backend.error',payload: undefined}) // going to resolve an error
        this.backend(tasks).then(
            ([tasks,values]) => { // passed tasks, last values from backend
                this.mainBoardStore.dispatch({type: 'backend.message', payload: undefined})
                this.loading = false // set only when complete
                // this action can be a cause for start new processing
                this.mainBoardStore.dispatch({ // mark processed tasks with undefined {0: undefined,1:undefined,...}
                    type: "backend.tasks",
                    payload: Object.keys(tasks).reduce((acc,index) => ({...acc,[index]:undefined}),{})
                })
                //console.group("resolveTasks response"); console.log(values); console.groupEnd()
                const booksList = this.mainBoardStore.getState().booksList
                Object.entries(Object.entries(values).reduce((acc,[name,value]) => { // update pages fields
                    if(value.book!==""
                        && value.book!==booksList.list[booksList.selectedBook].name) return acc // skip
                    const lastDot = name.lastIndexOf(".")
                    const path = name.substr(0, lastDot)
                    const field = name.substr(lastDot+1)

                    if(acc[path]) {
                        acc[path][field] = value.value
                        return acc
                    }

                    return {
                        ...acc,
                        [path]: {[field]: value.value}
                    }
                },{})).forEach(([path,fields])=>{
                    this.mainBoardStore.dispatch({
                        type: path, payload: fields
                    })
                })
            },
            (error)=>{
                console.group("backend"); console.error(error); console.groupEnd()
                const willResolveAt = Date.now()+this.backendRetryTime
                if(error.name === "TypeError")
                    this.mainBoardStore.dispatch({
                        type: 'backend.error',
                        payload: {message: "Can't connect to backend. Check connection.", willResolveAt: willResolveAt}
                    })
                else this.mainBoardStore.dispatch({type: 'backend.error', payload: {message: error.message, willResolveAt: willResolveAt}})
                setTimeout(this.resolveTasks.bind(this), this.backendRetryTime, tasks)
            })
    }
    backend = async (tasks) => { // {0: task, 1:task,...}, task {name: value}, is a command to get/set value
        //console.group("backend "+endpoint);console.log(tasks);console.groupEnd()
        const request = JSON.stringify(Object.values(tasks).reduce((acc,task)=>[...acc, task],[]))
        const json = await( await fetch(endpoint, {
            method: "post",
            credentials: "include",
            body: request
        })).json()
        if(json.error) throw new Error(json.error)
        return([tasks,json])
    }
}



