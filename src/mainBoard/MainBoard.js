import React, { Component } from 'react';
import { connect } from 'react-redux'
import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppPage from './appPages/AppPage'
import './MainBoard.css';

const tabs = [
    {name: "Help", },
    {name: "Settings",},
    {name: "DataProvider",title: "Data"},
]

class MainTabs extends Component {
    state = {
        tmpDisableTabs: false // needed only to resolve bug: selected tab underline not become updated
    }
    componentWillReceiveProps(nextProps) {// needed only to resolve bug: selected tab underline not become updated
        if(this.props.full!==nextProps.full) {
            setTimeout(this.setState.bind(this),900,{tmpDisableTabs:true}) // tansitions === 1s
        }
    }
    componentDidUpdate(nextProps,nextState) {// needed only to resolve bug: selected tab underline not become updated
        if(!nextState.tmpDisableTabs && this.state.tmpDisableTabs)
            this.setState({tmpDisableTabs: false})
    }
    render() {
        const props = this.props
        return <div>
            {!this.state.tmpDisableTabs ? <AppBar position="static" color="default" className="dynamicBackgroundColor">
                <Tabs
                    value={props.tab}
                    onChange={(event, value) => props.onTab(value)}
                    indicatorColor={"white"}
                    textColor="primary"
                    centered={props.full}
                    fullWidth
                >
                    {tabs.map((tab) => <Tab
                        key={tab.name}
                        label={tab.title?tab.title:tab.name}/>)}
                </Tabs>
            </AppBar> : ""}
            <SwipeableViews index={props.tab}>
                {tabs.map((tab) => <div key={tab.name} id={tab.name} className="appPage"><AppPage name={tab.name}/></div>)}
            </SwipeableViews>
        </div>
    }
}

class _MainBoard extends Component {
    state = {
        full: false
    }
    // shouldComponentUpdate(nextProps,nextState) {
    //     if(nextState!==this.props.state) return true
    //     if(nextProps.tab!==this.props.tab) return true
    //     if(nextProps.className!==this.props.className) {
    //         console.log(nextProps.className)
    //     }
    //     return false
    // }
    render() {
        const tab = this.props.tab!==undefined?this.props.tab:0
        const className = this.props.className?this.props.className:"odd"
        const props = this.props
        return <div id="mainBoard" className={className}
                    style={this.state.full?{
                        right: "0.5cm",
                        transition: "right 1s"
                    }:{}}>
            <div style={{
                position: "fixed",color: "white",fontWeight:"bold",fontFamily:"serif",
                zIndex: 2,
                right: "21.7cm",
                margin: 0,
                backgroundColor: props.className === "odd" ? "#0179BE" : "#FFC248",
                borderBottomRightRadius: 10,
                cursor: "pointer",
                ...this.state.full
                    ?{right:"0.1cm",transition:"right 1s"}
                    :{transition: "background-color 1s",},
            }} onClick={ e => this.setState({full: !this.state.full,tmpDisableTabs:true}) }>
                <div style={this.state.full
                    ?{transform: "rotate(180deg)",transition:"transform 2s ease-in-out",marginRight: 5}:
                    {transition:"transform 2s ease-in-ease-out"}
                }
                >&gt;&nbsp;</div>
            </div>
            <div className="inner">
                <div className="mainArea">
                    <MainTabs full={this.state.full} tab={tab} className={className} onTab={props.onTab}/>
                </div>
            </div>
        </div>
    }
}

export default connect(
    state => ({
        tab: state.mainBoard.tab,
        className: state.mainBoard.className
    }),
    dispatch => ({
        onTab: (tab) => {
            dispatch({type:'mainBoard.tab',payload: tab})
        }
    })
)(_MainBoard)

