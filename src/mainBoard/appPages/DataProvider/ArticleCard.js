/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
//import classnames from 'classnames';
import Card, { /*CardHeader,*/ CardMedia, CardContent, /*CardActions*/ } from 'material-ui/Card';
//import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
// import FavoriteIcon from 'material-ui-icons/Favorite';
// import ShareIcon from 'material-ui-icons/Share';
//import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
//import InfoIcon from 'material-ui-icons/Info';
import {connect} from 'react-redux'

const styles = theme => ({
    card: {
        width: 253,
    },
    media: {
        height: 177,
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    flexGrow: {
        flex: '1 1 auto',
    }
});

class ArticleCard extends React.Component {
    state = {
        clicked:false,
    }
    previewChanged = false
    componentWillReceiveProps(nextProps) { // store edited preview, for using when the card become selected again
        if(nextProps.previewData===undefined
        || this.props.previewData===undefined
        || this.props.previewData.id!==this.props.id) {
            return
        }

        if(nextProps.previewData.id!==this.props.previewData.id) { // an editing of the article finished - save previewData
            if(this.previewChanged) {// has changes
                this.props.savePreview(Object.entries(this.props.previewData)
                    .filter(([field]) => field !== 'section' && field !== 'page')
                    .reduce((acc, [field, value]) => ({...acc, [field]: value}), {}))
                this.previewChanged = false  // drop indicator of changes
            }
            //this.props.mainBoardTab(2, {preview: undefined})
            window.removeEventListener('keydown',this.catchEscape)
            this.wasEdited = false
            if(this.state.clicked) this.setState({clicked: false})
            return
        }
        if(!this.previewChanged) this.previewChanged
            = nextProps.previewData!==this.props.previewData && this.props.previewData.id===nextProps.previewData.id
    }
    static onMouseDown(event) {
        if(event.button===0) this.setState({clicked:true})
    }
    static onMouseUp() {
        if(this.props.previewData && this.props.previewData.id===this.props.id) {
            this.props.preview({
                section: undefined,
                page: undefined,
                id: undefined,
                header: undefined,
                title: undefined,
                content: undefined,
                footer: undefined
            })
            this.setState({clicked: false})
        } else {
            const scroll = this.props.scroll?this.props.scroll:{}
            this.props.preview({
                section: scroll.section
                    ? scroll.section
                    : (this.props.bookLayout.sections[0]?this.props.bookLayout.sections[0].section:undefined),
                page: scroll.page?scroll.page:0,
                id: this.props.id,
                ...(this.props.savedPreviewData && this.props.savedPreviewData[this.props.id]
                    ? this.props.savedPreviewData[this.props.id] // use previously saved previewData
                    : {                                          // construct previewData from article fields
                        header: this.props.image?"http://en.unesco.kz/_images/"+this.props.image.name:'',
                        title: (this.props.title.length>50?"":"<br>")+this.props.title,
                        content: this.props.fulltext,
                        footer: this.props.lang
                            ?'Подробнее: <a href="http://ru.unesco.kz/'
                            +this.props.readable_url+'">http://ru.unesco.kz/'+this.props.readable_url+
                            "</a>"
                            : 'See more information at: <a href="http://en.unesco.kz/'
                            +this.props.readable_url+'">http://en.unesco.kz/'+this.props.readable_url+
                            "</a>"
                    }),
            })
        }
    }
    shouldComponentUpdate(nextProps,nextState) {
        if(nextState!==this.state) return true   // clicked in/clicked out
        if(nextProps.lang!==this.props.lang) return true   // language changed
        return false
    }
    render() {
        const classes = this.props.classes
        return <div className={"outer_card"+(
                this.state.clicked
                    ? " clicked"
                    : "")}
                 onMouseDown={ArticleCard.onMouseDown.bind(this)}
                 onMouseUp={ArticleCard.onMouseUp.bind(this)}
                 >
                <Card className={classes.card}>
                    {this.props.image && <CardMedia
                        className={classes.media}
                        image={"http://en.unesco.kz/_images/"+this.props.image.preview2_name}
                        title={this.props.title}
                    />}
                    <CardContent>
                        <Typography component="p">
                            {this.props.title}
                        </Typography>
                    </CardContent>
                </Card>
            </div>
    }
}

ArticleCard.propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    fulltext: PropTypes.string,
    readable_url: PropTypes.string.isRequired,
    image: PropTypes.object,
    lang: PropTypes.number.isRequired,
};

export default connect(
    state => ({
        bookLayout: state.bookLayout,
        previewData: state.previewData,
        scroll: state.scroll,
        savedPreviewData: state.savedPreviewData,
    }),
    dispatch => ({
        preview: data => dispatch({
            type: "previewData",
            payload: data
        }),
        savePreview: data => dispatch({
            type: "savedPreviewData",
            payload: {[data.id]:data}
        }),
        message: message => {
            dispatch({
                type: "backend.error",
                payload: {message: message}
            })
            setTimeout(dispatch=>dispatch({ // close after 3 sec
                type: "backend",
                payload: {error: undefined}
            }),3000, dispatch)
        },
        mainBoardTab: (tab,parameters) => {
            dispatch({type:'mainBoard.tab',payload: tab})
            dispatch({type:'mainBoard.pages.Help',payload: parameters})
        },
        cancel: (preview) => {
            dispatch({
                type: "previewData",
                payload: Object.keys(preview)
                    .reduce((acc,field)=>({...acc,[field]: undefined}),{})
            })
        }
    })
)(withStyles(styles)(ArticleCard))