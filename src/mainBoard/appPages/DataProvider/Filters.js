/* eslint-disable flowtype/require-valid-file-annotation */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';
import Constants from '../../../constants';

const styles = theme => ({
    chip: {
        margin: theme.spacing.unit / 2,
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
})
/* Component for displaying selected filters */
const Filters = (props) =><div className={props.classes.row}>
    {props.filter.lang>0 && <Chip
        label={Constants.languages[props.filter.lang]}
        onRequestDelete={()=>props.onDelete({lang: 0})}
        className={props.classes.chip}
        />}
    {props.filter.themes.map(theme => {
        return (
            <Chip
                label={theme}
                key={theme}
                onRequestDelete={()=>props.onDelete({themes: props.filter.themes.filter(name=>name!==theme)})}
                className={props.classes.chip}
            />
        );
    })}
</div>

Filters.propTypes = {
    filter: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
}

export default withStyles(styles)(Filters);