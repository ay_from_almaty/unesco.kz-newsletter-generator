/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import Constants from '../../../constants'

const styles = theme => ({
    container: {
        display: 'block',
    },
    container2: {
        display: 'flex',
        flexWrap: "wrap",
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;


class Filter extends React.Component {
    render() {
        if(!this.props.themes) return null
        const classes = this.props.classes;
        const {languages} = Constants
        return (
            <div className={classes.container}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="themes_list">Themes</InputLabel>
                    <Select
                        className={this.props.className}
                        multiple
                        value={this.props.filter.themes}
                        onChange={event=>this.props.onChange({...this.props.filter,themes: event.target.value})}
                        input={<Input id="themes_list" />}
                        MenuProps={{
                            PaperProps: {
                                style: {
                                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                },
                            },
                        }}
                    >
                        {this.props.themes.map(name => (
                            <MenuItem
                                key={name}
                                value={name}
                            >
                                {name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <div className={classes.container2}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="lang">Language</InputLabel>
                        <Select
                            className={this.props.className}
                            value={languages[this.props.filter.lang]}
                            onChange={event=>this.props.onChange({...this.props.filter, lang: languages.indexOf(event.target.value)})} // lang===1 is Russian
                            input={<Input id="lang" />}
                            MenuProps={{
                                PaperProps: {
                                    style: {
                                        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                    },
                                },
                            }}
                        >
                            {languages.map(name => (
                                <MenuItem
                                    key={name}
                                    value={name}
                                >
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
            </div>
        )
    }
}

Filter.propTypes = {
    classes: PropTypes.object.isRequired,
    themes: PropTypes.array.isRequired,
    filter: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
}

export default withStyles(styles)(Filter)