// @flow weak
/* eslint-disable react/no-multi-comp */

import React,{Component} from 'react';
import PropTypes from 'prop-types';
// import { FormGroup, FormControlLabel } from 'material-ui/Form';
// import Checkbox from 'material-ui/Checkbox';
import { withStyles } from 'material-ui/styles';
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField';
// import Divider from 'material-ui/Divider';
// import Typography from 'material-ui/Typography'
//import Icon from 'material-ui/Icon';
// import IconButton from 'material-ui/IconButton';
// import MoreVertIcon from 'material-ui-icons/MoreVert';
// import DeleteIcon from 'material-ui-icons/Delete';
// import UpIcon from 'material-ui-icons/ArrowUpward';
// import DownIcon from 'material-ui-icons/ArrowDownward';
// import Menu, { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
import IconButton from 'material-ui/IconButton';
import classnames from 'classnames';
import ArticleCard from './ArticleCard'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import CheckIcon from 'material-ui-icons/Check';
import ErrorIcon from 'material-ui-icons/Error';
import HelpIcon from 'material-ui-icons/Help';
import CloudDownloadIcon from 'material-ui-icons/CloudDownload';
import Popover from 'material-ui/Popover';
import { findDOMNode } from 'react-dom';
import SetFilter from './SetFilter'
import Filters from './Filters'
import "./index.css"
import constants from "../../../constants";

const endpoint = "http://intranet.unesco.kz/update/newsletter/dataProvider.php"

const styles = theme => ({
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    wrapper: {
        position: "relative",
    },
    progress: {
        position: 'absolute',
        top: -2,
        left: -2,
    },
    textFieldDate: {
        width: 200,
        marginRight: constants.theme.spacing.unit,
    },
    textField: {
        width: "100%",
    },
})

class DataProvider extends Component {
    data = []
    isExpandAll = localStorage.getItem("DataProvider.expandAll")==="true"
    state = {
        expanded: {}, // set of group names of expanded groups
        loading: false,
        success: false,
        error: undefined,
        showFilterSetup: false,     // true - popover for filters opened
        filterAnchorEl: undefined,
        filter: {themes: [],lang: 0}, // selected themes, selected lang
        themes: [],           // all themes
        search: undefined,           // search string used as filter
        lang: 0                      // will set after receiving of the data from the server
    }

    toggleExpand = name => this.setState({expanded: {...this.state.expanded, [name]: !this.state.expanded[name]}})

    toggleExpandAll = data => {
        this.isExpandAll = !this.isExpandAll    // to expand all new groups
        localStorage.setItem("DataProvider.expandAll",this.isExpandAll)
        this.setState({expanded: data.reduce((acc,[name])=>({
            ...acc,
            [name]: this.isExpandAll
        }),{})})
    }

    search = event => {
        if(!this.searchInput) return

        if(this.searchTask) clearTimeout(this.searchTask)

        if(event.key === "Enter") { // search immediately
            this.setState({search: this.searchInput.value})
            return
        }

        this.searchTask = setTimeout(self => self.setState({search: this.searchInput.value}),1000,this)
    }

    download = () => {
        if (!this.state.loading) {
            this.setState({
                success: false,
                loading: true,
                error: undefined,
            });

            this.database(
                "?from=" + this.props.from
                + "&to=" + this.props.to
                + this.state.filter.themes.reduce((acc,theme)=>acc+"&theme[]="+encodeURI(theme),"") // to be GET array
                + (this.state.filter.lang?"&lang=1":"")
            ).then(
                (json) => {
                    this.data = Object.entries(json)
                    this.setState({                          // expand/collapse new groups
                        loading: false,
                        success: true,
                        expanded: this.data.filter(([name])=>this.state.expanded[name]===undefined).reduce((acc,[name])=>({
                            ...acc,
                            [name]: this.isExpandAll
                        }),this.state.expanded),
                        lang: this.state.filter.lang,
                    });
                    setTimeout(self => self.setState({success: false}), 1000, this)
                },
                (error) => {
                    console.group("DataProvider")
                    console.error(error)
                    console.groupEnd()
                    this.setState({error: error.toString(), loading: false})
                })
        }
    }

    setFilter = filter => {
        const newFilter =  {filter: {...this.state.filter, ...filter}}
        this.setState(newFilter);
        localStorage.setItem("filter", JSON.stringify(filter))
        if(!this.state.showFilterSetup) this.needDownload = true // no downloads, while filter popover is open
    }
    openFilterPopover() {
        this.filterBeforeSetup = this.state.filter
        this.setState({
            showFilterSetup: true,
            filterAnchorEl: findDOMNode(this.button),
            className: document.getElementById("mainBoard").className.indexOf("odd")?"even":"odd"
        })
    }
    closeFilterPopover() {
        if(this.filterBeforeSetup!==this.state.filter) this.download() // has changed filter
        this.setState({showFilterSetup: false})
    }
    componentDidUpdate(prevProps,prevState) {
        if(this.needDownload) {
            this.needDownload = false
            this.download()
        }
    }
    componentWillReceiveProps(nextProps) {
        if (!nextProps.from)
            this.props.setState({from: new Date(Date.now() - 20 * 24 * 60 * 60 * 1000 + 6 * 60 * 60 * 1000).toISOString().replace(/T.+$/, "")})
        if (!nextProps.to)
            this.props.setState({to: new Date(Date.now() + 6 * 60 * 60 * 1000).toISOString().replace(/T.+$/, "")})
    }
    componentDidMount() {
        if (!this.props.from)
            this.props.setState({from: new Date(Date.now() - 20 * 24 * 60 * 60 * 1000 + 6 * 60 * 60 * 1000).toISOString().replace(/T.+$/, "")})
        if (!this.props.to)
            this.props.setState({to: new Date(Date.now() + 6 * 60 * 60 * 1000).toISOString().replace(/T.+$/, "")})

        this.downloadThemes()
    }
    downloadThemes() { // download list of themes
        if (!this.state.loading) {
            this.setState({
                success: false,
                loading: true,
                error: undefined,
            });

            this.database("?themes=1").then(
                (json) => {
                    const filter = localStorage.getItem("filter")
                    this.setState({
                        themes: json,
                        filter: filter ? {...this.state.filter, ...JSON.parse(filter)} : this.state.filter,
                        loading: false,
                        success: true,
                    });
                    setTimeout(self => self.setState({success: false}), 1000, this)
                },
                (error) => {
                    console.group("DataProvider")
                    console.error(error)
                    console.groupEnd()
                    this.setState({error: error.toString(), loading: false})
                })
        }
    }

    render = () => {
        // console.group("render DataProvider")
        // console.log(this.props)
        // console.groupEnd()
        let hasData = false
        const data = this.state.search                                           // filter if search string defined
            ? this.data.reduce((acc, [month, data]) => {
                hasData = true
                const search = this.state.search.toLowerCase()
                const fieldMatch = (field) => field && field.toLowerCase().indexOf(search)!==-1
                return [  // reconstruct object without non-matched
                    ...acc, [
                        month,
                        Object.entries(data).reduce((acc2,[sector, sector_data])=>([
                            ...acc2,
                            sector_data.filter(article =>            // filter here, list of articles inside sector
                                fieldMatch(article.title)
                                || fieldMatch(article.description)
                                || fieldMatch(article.fulltext)
                                // search in always available English fields
                                || fieldMatch(article.title_en)
                                || fieldMatch(article.description_en)
                                || fieldMatch(article.fulltext_en))
                        ]),[])
                    ]
                ]},[]).filter(([month, data])=>                                   // filter empty months
                    Object.values(data).filter(sector_data=>sector_data.length>0).length>0)
            : this.data
        if(!hasData) hasData = data.length>0
        let firstMonth = 0
        return <div style={{paddingTop: "5px", paddingBottom: "5px"}}>
            <div style={{display: "flex", flexWrap: "wrap", marginBottom: "10px"}}>
                <TextField
                    id="from"
                    label="From"
                    type="date"
                    value={this.props.from}
                    className={this.props.classes.textFieldDate}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={(event) => this.props.setState({from: event.target.value})}
                />
                <TextField
                    id="to"
                    label="To"
                    type="date"
                    value={this.props.to}
                    className={this.props.classes.textFieldDate}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={(event) => this.props.setState({to: event.target.value})}
                />
                <div style={{flex:1,display:"flex",justifyContent:"space-between"}}>
                    <div style={{flex:1,display:"flex"}}><div className={this.props.classes.wrapper} style={{alignSelf: "center",marginTop:5,marginRight: constants.theme.spacing.unit}}>
                        <Button disabled={this.state.loading} raised ref={(el) => {
                            this.button = el
                        }} onClick={this.openFilterPopover.bind(this)}>
                            {this.state.filter.themes.length>0||this.state.filter.lang>0?"Edit":"Add"} filter
                        </Button>
                        <Popover className={"popover "+this.state.className}
                                 open={this.state.showFilterSetup}
                                 anchorEl={this.state.filterAnchorEl}
                                 onRequestClose={this.closeFilterPopover.bind(this)}
                                 anchorOrigin={{
                                     vertical: 'bottom',
                                     horizontal: 'center',
                                 }}
                                 transformOrigin={{
                                     vertical: 'top',
                                     horizontal: 'center',
                                 }}>
                            <SetFilter themes={this.state.themes}
                                       filter={this.state.filter}
                                       onChange={this.setFilter}
                                       className="select"/>
                        </Popover>
                    </div>
                    <div className={this.props.classes.wrapper}>
                        <Button fab color="primary"
                                onClick={this.download}>
                            {this.state.error
                                ? <ErrorIcon/>
                                : this.state.success ? <CheckIcon/> : <CloudDownloadIcon/>}
                        </Button>
                        {this.state.loading && <CircularProgress size={60} className={this.props.classes.progress}/>}
                    </div></div>
                    <div className={this.props.classes.wrapper}>
                        <div className="help"><Button fab raised
                                                                               className="help_button"
                                                                               onClick={this.props.showHelp}>
                            <HelpIcon/>
                        </Button></div>
                    </div>
                </div>
            </div>
            {this.state.error && <div className="dynamicColor" style={{fontSize: 21}}>{this.state.error}</div>}
            { (this.state.filter.themes.length>0||this.state.filter.lang>0) && <Filters
                filter={this.state.filter}
                onDelete={this.setFilter} />
            }
            {hasData && <TextField
                id="data_search"
                label="SEARCH - Type text here to filter an articles that contains this text"
                className={this.props.classes.textField}
                defaultValue=""
                inputRef={input=>{this.searchInput = input}}
                margin="normal"
                onKeyDown={this.search}
            />}
            { data.map(([month, _data]) => <div key={month}>
                <IconButton
                    className={classnames(this.props.classes.expand, {
                        [this.props.classes.expandOpen]: this.state.expanded[month] ? true : false,
                    })}
                    onClick={() => this.toggleExpand(month)}
                    aria-expanded={this.state.expanded[month]}
                    aria-label="Show more"
                >
                    <ExpandMoreIcon/>
                </IconButton>
                <span className="dynamicColor"
                      style={{cursor: "pointer"}}
                      onClick={() => this.toggleExpand(month)}>{month}</span>
                { (firstMonth++)===0 && <div style={{float: "right"}}>
                    <Button raised onClick={this.toggleExpandAll.bind(this,data)}>{!this.isExpandAll?"Expand":"Collapse"} all</Button>
                </div> }
                <Collapse in={this.state.expanded[month]} transitionDuration="auto" unmountOnExit>
                    <div className="cardsBoard">
                        {Object.entries(_data).map(([sector, sector_data]) => sector_data
                            .map(article =>
                                <div key={article.id}>
                                    <ArticleCard lang={this.state.lang} {...article}/>
                                </div>))}
                    </div>
                </Collapse>
            </div>)}</div>
    }
    database = async (urlParameters) => {
        const json = await( await fetch(endpoint+(urlParameters?urlParameters:""), {
            method: "get",
            credentials: "include",
        })).json()
        if(json.error) throw new Error(json.error)
        return(json)
    }
}

DataProvider.propTypes = {
    setState: PropTypes.func.isRequired,
}

export default connect(
    state => ({}),
    dispatch => ({
        showHelp: () => dispatch({ type: "help",payload: "DataProvider" })
    })
)(withStyles(styles)(DataProvider))