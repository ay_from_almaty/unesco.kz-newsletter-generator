    // @flow weak
/* eslint-disable react/no-multi-comp */

import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import './Help.css'
import PropTypes from 'prop-types';
import Editing from '../../help/texts/Editing';
import HelpIcon from 'material-ui-icons/Help';

// 1: overall help
// 2: help related editing
// 3: help related preview
const Help = (props) => {
    //console.log("help render "+JSON.stringify(props))
    return <div id="Help">
        <SwipeableViews index={props.editPageField?1:(props.preview?2:0)}>
            <div>
                <p>Here you can create a newsletter suitable for printing or saving in "pdf".<br/>
                    After editing of the document is finished select print in browser <br/>
                    Screenshots:&nbsp;<a href="help/print_chrome_1.jpg" target="_blank">Chrome&nbsp;1</a>
                    &nbsp;<a href="help/print_chrome_2.jpg" target="_blank">Chrome&nbsp;2</a>
                    &nbsp;<a href="help/print_firefox_1.jpg" target="_blank">Firefox&nbsp;1</a>
                    &nbsp;<a href="help/print_firefox_2.jpg" target="_blank">Firefox&nbsp;2</a>
                    &nbsp;<a href="help/print_firefox_3.jpg" target="_blank">Firefox&nbsp;3</a>
                    .</p>
                <ol>
                    <li>Create the new book and sections on SETTINGS tab</li>
                    <li>Fill the pages with website data on DATA tab</li>
                    <li>Check and edit a some fields</li>
                    <li>Print</li>
                </ol>
                <ul>
                    <li>You can get help on every tab by clicking <HelpIcon/></li>
                    <li>You can have many books. <br/>For example, "Newsletter in English" and "Newsletter in Russian"</li>
                    <li>On the right, you can edit pages</li>
                    <li>From the top you can select a section or page in the current section.</li>
                    <li>After selecting, the area will scroll to the desired section / page.</li>
                    <li>The page is divided into areas that can be edited.</li>
                    <li>The upper area is intended for image storage, just drag image
                        from desktop, explorer or finder (Mac OS).</li>
                    <li>The remaining regions can contain any formatted text.</li>
                    <li>Just click on the field and edit the text as you like.<br/>
                        Hot keys work in analogy with the Word and other editors (Control-b, Control-i, ...).<br/>
                        Also, Control-z and Control-y work.</li>
                </ul>
                Some videos which shows work with application:
                <ul>
                    <li><a rel="noopener noreferrer" href="https://drive.google.com/file/d/0B7SJ2q4GS5n6TFU5UkEzUl9tRk0/view?usp=sharing"
                           target="_blank">Creating the English newsletter video</a>
                    </li>
                    <li><a rel="noopener noreferrer" href="https://drive.google.com/file/d/0B7SJ2q4GS5n6UW43bkV0VU5oSlE/view?usp=sharing"
                           target="_blank">Creating the Russian newsletter video</a>
                    </li>
                </ul>
                <p align="right"><a rel="noopener noreferrer" href="https://bitbucket.org/ay_from_almaty/unesco.kz-newsletter-generator"
                                    target="_blank">About program, source code</a></p>
            </div>
            <Editing/>
        </SwipeableViews>
    </div>
}

Help.propTypes = {
    setState: PropTypes.func.isRequired,
}

export default Help