// @flow weak
/* eslint-disable react/no-multi-comp */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Settings from './Settings/Settings'
import DataProvider from './DataProvider/index'
import Help from './Help'
import PropTypes from 'prop-types';
import './AppPage.css'

class AppPage extends Component {
    setState(state) { this.props.setState(this.props.name, state) } // use one method for all pages
    shouldComponentUpdate(nextProps,nextState) { // just relax. render() concrete page only if state for page changed
        // update only page which is target for props.
        if(nextProps[this.props.name]!==this.props[this.props.name]) {
            return true
        }
        return false
    }
    render() {
        const props = this.props[this.props.name]?this.props[this.props.name]:{}
        switch(this.props.name) {
            case "Settings": return <Settings {...props} setState={this.setState.bind(this)}/>
            case "DataProvider": return <DataProvider {...props} setState={this.setState.bind(this)}/>
            case "Help": return <Help {...props} setState={this.setState.bind(this)}/>
            default: return <div>Page '{this.props.name}' not realized. Modify <i>'AppPage/render()'</i></div>
        }
    }
}

AppPage.propTypes = {
    name: PropTypes.string.isRequired,
}

export default connect(
    state => (state.mainBoard.pages),
    dispatch => ({
        setState: (page,state) => {
            dispatch({type:'mainBoard.pages.'+page, payload:state})
        }
    })
)(AppPage)
