// @flow weak
/* eslint-disable react/no-multi-comp */

import React,{Component} from 'react';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';
import { withStyles } from 'material-ui/styles';
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField';
//import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography'
//import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui-icons/MoreVert';
import DeleteIcon from 'material-ui-icons/Delete';
import UpIcon from 'material-ui-icons/ArrowUpward';
import DownIcon from 'material-ui-icons/ArrowDownward';
import Menu, { MenuItem } from 'material-ui/Menu';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import HelpIcon from 'material-ui-icons/Help';
import SettingsIcon from 'material-ui-icons/Settings';
import EditBooks from './EditBooks'
import EditSectionPopover from './EditSectionPopover'
import constants from '../../../constants'

const styles = theme => ({
    textFieldSection: {
        minWidth: 200,
        width: "100%"
    },
    textFieldPagesCount: {
        marginLeft: constants.theme.spacing.unit,
        minWidth: "50",
    },
    title: {
        textAlign: "center",
    },
    wrapper: {
        position: "relative",
    },
});

class Settings extends Component {
    state = {
        editPagesCount: undefined,
        menuAnchor: undefined,
        openMenu: false,
        openSectionPopover: true,
        popoverSectionIndex: undefined,
        sectionIndex: undefined,
        locked: false,
        pagesCount: 5,
        sectionError: undefined,
    }
    inputs = {}
    componentWillReceiveProps(nextProps) {
        if(this.state.locked && nextProps.bookLayout !== this.props.bookLayout) {
            this.setState({locked:false,sectionError:undefined,pagesCount:5})
            if(this.newSectionInput) {
                this.newSectionInput.value = ""
                this.newSectionInput.blur()
            }
        }
    }
    shouldComponentUpdate(nextProps, nextState) { // to filter backend && bookData && booksList changes
        if(this.props.copyBook!==nextProps.copyBook) return true
        if(this.state !== nextState) return true                         // state changed
        if(this.props.bookLayout !== nextProps.bookLayout) return true   // layout changed
        if(this.props.showBorders !== nextProps.showBorders) return true // page field 'showBorders' changed
        return false
    }
    openMenu = (event,sectionIndex) => {
        this.setState({ openMenu: true, menuAnchor: event.currentTarget, sectionIndex: Number(sectionIndex) })
    }
    closeMenu = (locked) => {
        this.setState({ openMenu: false, sectionIndex: undefined, locked: locked===true?locked:this.state.locked })
    }
    itemUp = () => {
        if(this.state.sectionIndex !== undefined
             && this.state.sectionIndex-1>=0) {
            this.props.moveSection(this.state.sectionIndex, this.state.sectionIndex-1, this.props)
            this.closeMenu(true)
        } else {
            this.closeMenu()
        }
    }
    itemDown = () => {
        if(this.state.sectionIndex !== undefined
            && this.state.sectionIndex<this.props.bookLayout.lastSectionIndex) {
            this.props.moveSection(this.state.sectionIndex, this.state.sectionIndex+1, this.props)
            this.closeMenu(true)
        } else {
            this.closeMenu()
        }
    }
    itemDelete = () => {
        if(this.state.sectionIndex !== undefined) {
            this.props.deleteSection(this.state.sectionIndex,this.props)
            this.closeMenu(true)
        } else {
            this.closeMenu()
        }
    }
    itemSettings = () => {
        if(this.state.sectionIndex !== undefined) {
            this.setState({openSectionPopover: true, popoverSectionIndex: this.state.sectionIndex})
            this.closeMenu()
        } else {
            this.closeMenu()
        }
    }    
    newSectionKeyDown = (event) => {
        if(!this.newSectionInput) return
        if(event.key === "Enter") {
            if(this.newSectionInput.value.length===0) {
                this.setState({sectionError:" - Please fill"})
                return
            }
            this.setState({locked:true,sectionError:undefined})
            this.props.addSection(this.newSectionInput.value,this.state.pagesCount,this.props)
        } else if(event.key === "Escape") {
            this.newSectionInput.value = ""
            window.getSelection().removeAllRanges()
            this.setState({pagesCount:5})
            event.target.blur()
        }
    }
    editSection = (event) => {
        if(this.state.sectionIndex === undefined) return // stop infinite loop of "blurring"
        if(event.key === "Enter") {
            const index = this.state.sectionIndex
            if(this.inputs[index] && this.inputs[index].value.length>0) {
                this.props.editSection(
                    index,
                    {section: this.inputs[index].value, pagesCount: this.state.editPagesCount},
                    this.props
                )
            }
            window.getSelection().removeAllRanges()
            this.setState({sectionIndex: undefined,editPagesCount:undefined})
            event.target.blur()
        } else if(event.key === "Escape") {
            const index = this.state.sectionIndex
            if(this.inputs[index])
                this.inputs[index].value = this.props.bookLayout.sections[index].section
            window.getSelection().removeAllRanges()
            this.setState({sectionIndex: undefined,editPagesCount:undefined})
            event.target.blur()
        }
    }
    render() {
        const props = this.props
        const state = this.state
        return <div>
                <Typography type="title" className={props.classes.title} style={{marginBottom: constants.theme.spacing.unit}}>
                    <div className="dynamicColor">Manage books</div>
                </Typography>
                <div style={{display:"flex",justifyContent: "space-between",paddingBottom:5}}>
                    <EditBooks setState={props.setState} copyBook={props.copyBook}/>
                    <div className="help">
                        <Button raised fab
                                className="help"
                                onClick={this.props.showHelp}>
                            <HelpIcon/>
                        </Button>
                    </div>
                </div>
                {props.bookLayout.lastSectionIndex!==null && <div>
                    <Typography type="title" className={props.classes.title} style={{marginTop: constants.theme.spacing.unit*1.5,}}>
                        <div className="dynamicColor">Edit sections and count of pages. <i style={{fontSize: "80%"}}>Enter - save, Esc - cancel</i></div>
                    </Typography>
                    <div style={{
                        marginBottom: constants.theme.spacing.unit,
                        ...state.locked
                        ? {opacity: 0.5, transition: "opacity 1s", pointerEvents: "none"}
                        : {opacity: 1, transition: "opacity 1s", pointerEvents: "auto"}
                    }}>
                        <div>
                            <div style={{display: "flex",flexWrap: 'nowrap',}} noValidate>
                                    <TextField
                                        error={state.sectionError!==undefined?true:false}
                                        id="new_section"
                                        label={"New section name"+(state.sectionError!==undefined?state.sectionError:"")}
                                        className={props.classes.textFieldSection}
                                        defaultValue=""
                                        inputRef={input=>{this.newSectionInput = input}}
                                        margin="normal"
                                        onKeyDown={this.newSectionKeyDown}
                                    />
                                    <TextField
                                        id="new_section_pages_count"
                                        label="Pages"
                                        className={props.classes.textFieldPagesCount}
                                        value={state.pagesCount}
                                        onClick={(event) => {event.target.select()}}
                                        margin="normal"
                                        onChange={(event) => {
                                            if(/^\d+$/.test(event.target.value) && event.target.value>0 && event.target.value.length===1)
                                                this.setState({pagesCount: event.target.value})
                                        }}
                                        onKeyUp={(event) => {event.target.select()}}
                                        onKeyDown={this.newSectionKeyDown}
                                    />
                                <div style={{visibility:"hidden"}}>
                                    <IconButton className={props.classes.button} disabled>
                                        <MoreVertIcon/>
                                    </IconButton>
                                </div>
                            </div>
                            {props.bookLayout ? Object.entries(props.bookLayout.sections)
                                .filter(([,sectionLayout])=>sectionLayout) // filter undefined, any case
                                .map(([n,sectionLayout]) =>
                                    <div key={sectionLayout.section} style={{display: "flex",flexWrap: "nowrap"}}>
                                        <TextField
                                            id={sectionLayout.section}
                                            label=""
                                            className={props.classes.textFieldSection}
                                            defaultValue={sectionLayout.section}
                                            margin="normal"
                                            inputRef={input=>{ this.inputs[n] = input}}
                                            disabled={state.sectionIndex?state.sectionIndex!==n:true}
                                            onClick={event=>{
                                                this.setState({
                                                    editPagesCount:sectionLayout.pagesCount,
                                                    sectionIndex: n
                                                })
                                                setTimeout((target)=>target && target.focus(),100,event.target)
                                            }}
                                            onBlur={event=>{
                                                this.editSection({...event,key:"Escape"})
                                            }}
                                            onKeyDown={this.editSection}
                                        />
                                        <TextField
                                            id={sectionLayout.section+".pagesCount"}
                                            label=""
                                            className={props.classes.textFieldPagesCount}
                                            value={state.editPagesCount!==undefined && state.sectionIndex===n?state.editPagesCount:sectionLayout.pagesCount}
                                            margin="normal"
                                            onChange={(event) => {
                                                if(/^\d+$/.test(event.target.value) && event.target.value>0 && event.target.value.length===1)
                                                    this.setState({editPagesCount: event.target.value})
                                            }}
                                            disabled={state.sectionIndex?state.sectionIndex!==n:true}
                                            onClick={event=>{
                                                this.setState({
                                                    editPagesCount:sectionLayout.pagesCount,
                                                    sectionIndex: n
                                                })
                                                setTimeout((target)=>target.select(),100,event.target)
                                            }}
                                            onBlur={event=>{
                                                this.editSection({...event,key:"Escape"})
                                            }}
                                            onKeyUp={(event) => {event.target.select()}}
                                            onKeyDown={this.editSection}
                                        />
                                        <div>
                                            <IconButton
                                                aria-owns={this.state.openMenu ? "menu-"+n : null}
                                                aria-haspopup="true"
                                                onClick={(e) => this.openMenu(e,n)}
                                                className={props.classes.button} aria-label="Menu">
                                                <MoreVertIcon/>
                                            </IconButton>
                                            {this.renderSectionMenu(Number(n))}
                                        </div>
                                    </div>
                            ) : ""}
                        </div>
                    </div>
                    <div>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        value="showBorders"
                                        onChange={(event, checked) => props.setState({showBorders: checked})}
                                        checked={props.showBorders ? true : false}
                                    />
                                }
                                label="Show areas in the pages"
                            />
                        </FormGroup>
                    </div>
                    {this.state.popoverSectionIndex!==undefined
                    && this.props.bookLayout.sections[this.state.popoverSectionIndex]
                    && <EditSectionPopover open={this.state.openSectionPopover}
                         onRequestClose={()=>this.setState({openSectionPopover: false})}
                         anchorEl={this.state.menuAnchor} // use same anchor as menu
                         onChange={(sectionLayout,applyToAll)=>applyToAll
                             ? Object.values(this.props.bookLayout.sections)
                                 .forEach((_sectionLayout)=>
                                     props.editSection(
                                         undefined,
                                         {..._sectionLayout, // original section layout
                                             ...sectionLayout, // merge new layout
                                             section: _sectionLayout.section}, // merge section name to original again
                                         props))
                             : props.editSection(
                                 undefined,
                                 sectionLayout,
                                 props)
                         }
                         sectionLayout={this.props.bookLayout.sections[this.state.popoverSectionIndex]}/>}
                </div>}

            </div>
    }
    renderSectionMenu = (sectionIndex) =><Menu
        id={"menu-"+sectionIndex}
        anchorEl={this.state.menuAnchor}
        open={this.state.openMenu && this.state.sectionIndex===Number(sectionIndex)}
        onRequestClose={this.closeMenu}>
        {sectionIndex>0 && <MenuItem onClick={this.itemUp}><UpIcon/></MenuItem>}
        <MenuItem onClick={this.itemSettings}><SettingsIcon/></MenuItem>
        <MenuItem onClick={this.itemDelete}><DeleteIcon/></MenuItem>
        {sectionIndex<Number(this.props.bookLayout.lastSectionIndex) && <MenuItem onClick={this.itemDown}><DownIcon/></MenuItem>}
    </Menu>
}

Settings.propTypes = {
    setState: PropTypes.func.isRequired,
}


export default connect(
    state => ({
        bookLayout: state.bookLayout,
        backend: state.backend,
        bookData: state.bookData,
        booksList: state.booksList,
    }),
    dispatch => ({
        moveSection: (oldSectionIndex, newSectionIndex, state) => { // swap
            dispatch({ // we need perspective update here, to avoid duplicate names of section in intermediate state
                type: "bookLayout.sections",
                payload: {
                    [oldSectionIndex]: state.bookLayout.sections[newSectionIndex],
                    [newSectionIndex]: state.bookLayout.sections[oldSectionIndex]
                }
            })
            let taskIndex = state.backend.lastTaskIndex
            dispatch({
                type: "backend.tasks",
                payload: {                  // doing manually - we have only 2 fields
                    [++taskIndex]: {
                        ["bookLayout.sections."+oldSectionIndex+".section"]:     // old name -> new name
                        state.bookLayout.sections[newSectionIndex].section
                    },
                    [++taskIndex]: {
                        ["bookLayout.sections."+oldSectionIndex+".pagesCount"]:  // old pagesCount -> new pagesCount
                        state.bookLayout.sections[newSectionIndex].pagesCount
                    },
                    [++taskIndex]: {
                        ["bookLayout.sections."+newSectionIndex+".section"]:     // new name -> old name
                        state.bookLayout.sections[oldSectionIndex].section
                    },
                    [++taskIndex]: {
                        ["bookLayout.sections."+newSectionIndex+".pagesCount"]:  // new pagesCount -> old pagesCount
                        state.bookLayout.sections[oldSectionIndex].pagesCount
                    }
                }
            })
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
        },
        deleteSection: (sectionIndex, state) => {
            let taskIndex = state.backend.lastTaskIndex
            if(sectionIndex === state.bookLayout.lastSectionIndex) {
                dispatch({                                   // decrease section's index
                    type: "backend.tasks",
                    payload: {
                        [++taskIndex]: {
                            "bookLayout.lastSectionIndex": (state.bookLayout.lastSectionIndex-1)
                        } // shift index
                    }
                })
            } else {                                             // shift UP all followed sections
                const len = state.bookLayout.lastSectionIndex - sectionIndex
                dispatch({                                   // save shifted sections in backend
                    type: "backend.tasks",
                    payload: {
                        [++taskIndex]: {  // #1 shift index
                            "bookLayout.lastSectionIndex": (state.bookLayout.lastSectionIndex-1)},
                        ...new Array(len).fill(undefined).reduce((acc,nothing,n)=>{
                            const si = state.bookLayout.lastSectionIndex-n   // #2 REVERSE order of changes
                            return {
                                ...acc,
                                ...constants.sectionLayout.reduce((acc2,field)=>({
                                    ...acc2,
                                    [++taskIndex]: {
                                        ["bookLayout.sections."+(si-1)+"."+field]:
                                            state.bookLayout.sections[si][field]!==undefined
                                            ? state.bookLayout.sections[si][field]
                                            : ""},
                                }),{})
                            }
                         },{})}
                })
            }
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
        },
        addSection: (name, pagesCount, state) =>  {
            let taskIndex = state.backend.lastTaskIndex + 1
            let sectionEntry = Object.entries(state.bookLayout.sections)
                .filter(([n,sectionLayout])=>sectionLayout) // filter undefined, any case
                .find(([n,value])=>value.section===name)
            if(sectionEntry) {
                if(sectionEntry[1].pagesCount===pagesCount) return // no changes
                dispatch({
                    type: 'backend.tasks',
                    payload: {
                        ...state.backend.tasks,
                        [taskIndex]: {["bookLayout.sections."+sectionEntry[0]+".pagesCount"]: pagesCount},
                    }
                })
            } else {
                const sectionIndex = state.bookLayout.lastSectionIndex===""
                    ? 0
                    : Number(state.bookLayout.lastSectionIndex) + 1 // increase index of last section
                dispatch({
                    type: 'backend.tasks',
                    payload: {
                        [taskIndex++]: {["bookLayout.sections."+sectionIndex+".section"]: name},
                        [taskIndex++]: {["bookLayout.sections."+sectionIndex+".pagesCount"]: pagesCount},
                        [taskIndex]: {"bookLayout.lastSectionIndex": sectionIndex},
                    }
                })
            }
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
        },
        editSection: (sectionIndex, sectionLayout, state) =>  {
            const name = sectionLayout.section
            let taskIndex = state.backend.lastTaskIndex
            let sectionEntry = Object.entries(state.bookLayout.sections)
                .find(([n,value])=>value&&value.section===name)
            const index = sectionEntry?sectionEntry[0]:sectionIndex
            if(index===undefined || !state.bookLayout.sections[index]) return // just skip - section was deleted
            const oldName = state.bookLayout.sections[index].section
            const newSectionLayout = {...state.bookLayout.sections[index],...sectionLayout} // merge
            const tasks = {
                type: 'backend.tasks',
                payload: {
                    ...Object.entries(newSectionLayout) // just update of layout of section fully
                        .reduce((acc,[field,value])=>({
                            ...acc,
                            [++taskIndex]: {["bookLayout.sections."+index+"."+field]: value},
                        }),{}),
                    ...(oldName!==name ? {              // section name changed
                        ...(state.bookData && state.bookData[oldName] ? { // save fields for section with new name
                            ...Object.entries(state.bookData[oldName]).reduce((acc,[page,fields])=>{
                                return {
                                    ...acc,
                                    ...Object.entries(fields).reduce((acc2,[field,value])=>{
                                        return {
                                            ...acc2,
                                            [++taskIndex]: {["bookData."+name+"."+page+"."+field]: value}
                                        }
                                    },{})
                                }
                            },{})
                        }:{})
                    }:{}),
                }
            }
            dispatch(tasks)
            state.backend.lastTaskIndex = taskIndex // hack: mutate index until new state not came
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
            dispatch({       // perspective rendering - not handled by preMiddleware
                type:"bookLayout.sections",
                payload: {[index]: newSectionLayout} // merge
            })
        },
        showHelp: () => dispatch({ type: "help",payload: "Layout" })
    })
)(withStyles(styles)(Settings))