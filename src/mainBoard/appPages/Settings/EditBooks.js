/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import {connect} from 'react-redux'
import Button from 'material-ui/Button';
import Popover from 'material-ui/Popover';
import { findDOMNode } from 'react-dom';
import TextField from 'material-ui/TextField';
import constants from '../../../constants'
import Typography from 'material-ui/Typography'
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';

const styles = theme => ({
    textField: {
        minWidth: 400,
    },
    container: {
        display: 'flex',
        flexWrap: "nowrap",
    },
    formControl: {
        marginTop: constants.theme.spacing.unit/4,
        display: 'flex',
        flexWrap:"wrap",
        width: "300px",
        marginRight: constants.theme.spacing.unit
    },
    buttonOuter: {
        alignSelf: "center"
    },
    button: {
        marginTop: constants.theme.spacing.unit/2,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;


class EditBooks extends React.Component {
    buttons = {
        new: undefined,
        rename: undefined,
        delete: undefined,
    }
    bookNameInput = undefined
    state = {
        action: undefined, // new / rename / delete
        showPopover: false,
        anchorEl: undefined,
        className: "odd",
        error: undefined
    }
    shouldComponentUpdate(nextProps, nextState) { // to filter props.backend && props.bookData changes
        if(this.props.copyBook!==nextProps.copyBook) return true
        if(this.state !== nextState) return true                         // state changed
        if(this.props.booksList !== nextProps.booksList) return true     // list of books changed
        return false
    }
    openPopover = (action) => this.setState({
            action: action,
            showPopover: true,
            anchorEl: findDOMNode(this.buttons[action]),
            className: (document.getElementById("mainBoard").className.indexOf("odd")
                ? "even"
                : "odd") + (action==="delete"?" warning":"")
    })
    closePopover = () => this.setState({showPopover: false,error: undefined})
    render() {
        if(this.props.booksList.selectedBook===undefined) return <Typography style={{alignSelf:"center"}} type="title">
            <div className="dynamicColor">Loading ...</div>
        </Typography>

        const classes = this.props.classes
        const selectedBook = Number(this.props.booksList.selectedBook)
        const books = Object.values(this.props.booksList.list)
                .filter(book=>book!==undefined) // filter undefined, any case
                .reduce((acc,book)=>[...acc,book.name],[])
        const book = books[selectedBook]
        return (
            <div className={classes.container} style={
                this.props.booksList.selectedBook===null
                    ? {opacity: 0.5, transition: "opacity 1s", pointerEvents: "none"}
                    : {opacity: 1, transition: "opacity 1s", pointerEvents: "auto"}
            }>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="books_list">{books.length===0?<span style={{color:"darkgray"}}>No books</span>:"Current Book"}</InputLabel>
                    <Select
                        className="select"
                        value={book?book:""}
                        disabled={books.length===0}
                        onChange={event=>this.props.selectBook(event.target.value, this.props)}
                        input={<Input id="books_list" />}
                        MenuProps={{
                            PaperProps: {
                                style: {
                                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                },
                            },
                        }}
                    >
                        {books.map(name => (
                            <MenuItem
                                key={name}
                                value={name}
                            >
                                {name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <div className={classes.buttonOuter}>
                    <Button raised className={classes.button} ref={(el) => {
                        this.buttons.new = el
                    }} onClick={this.openPopover.bind(this,'new')} style={{marginRight: constants.theme.spacing.unit/2}}>
                        New
                    </Button>
                    {book ? <span>
                        <Button raised className={classes.button} ref={(el) => {
                            this.buttons.rename = el
                        }} onClick={this.openPopover.bind(this,'rename')} style={{marginRight: constants.theme.spacing.unit/2}}>
                            Rename
                        </Button>
                        <Button raised className={classes.button} ref={(el) => {
                            this.buttons.delete = el
                        }} onClick={this.openPopover.bind(this,'delete')} style={{marginRight: constants.theme.spacing.unit/2}}>
                            Delete
                        </Button>
                    </span>:""}
                    <Popover className={"popover "+this.state.className}
                             open={this.state.showPopover}
                             anchorEl={this.state.anchorEl}
                             onRequestClose={this.closePopover}
                             anchorOrigin={{
                                 vertical: 'bottom',
                                 horizontal: 'center',
                             }}
                             transformOrigin={{
                                 vertical: 'top',
                                 horizontal: 'center',
                             }}>
                        <div style={{display: "flex",flexWrap: 'nowrap',}} noValidate>
                            {this.state.action==="delete"
                                ? <div><Typography style={{alignSelf:"center"}} type="title">
                                    Are you sure to remove the book <b>{book}</b>?
                                </Typography><div style={{display: "flex",flexWrap:"nowrap"}}><Button raised
                                                     style={{
                                                         marginRight: constants.theme.spacing.unit/2,
                                                         marginTop: constants.theme.spacing.unit/2,
                                                     }}
                                                     onClick={()=>{
                                                         this.props.deleteBook(book, this.props)
                                                         this.closePopover()
                                                     }}>
                                    YES
                                </Button><Button raised
                                                 style={{
                                                     marginTop: constants.theme.spacing.unit/2,
                                                     width:"100%",
                                                 }}
                                                 onClick={this.closePopover}>
                                    No
                                </Button></div></div>
                                : this.renderNewPopover(book)}
                        </div>
                    </Popover>
                </div>
            </div>
        )
    }
    renderNewPopover = (book) => <div>
        <TextField
            error={this.state.error!==undefined}
            id="book_name"
            label={"The book name"+(this.state.error!==undefined?this.state.error:"")}
            className={this.props.classes.textField}
            defaultValue={this.state.action==="rename"?book:""} // show current book for rename
            inputRef={input=>{this.bookNameInput = input}}
            margin="normal"
            onKeyDown={this.bookNameKeyDown}/>
        {this.state.action==="new" && book && <FormGroup row>
            <FormControlLabel
                control={
                    <Checkbox
                        value="copyBook"
                        onChange={(event, checked) => this.props.setState({copyBook: checked})}
                        checked={this.props.copyBook ? true : false}
                    />
                }
                label={"Copy the book '"+book+"'"}
            />
        </FormGroup>}
    </div>

    bookNameKeyDown = (event) => {
        if(!this.bookNameInput) return
        if(event.key === "Enter") {
            if(this.bookNameInput.value.length===0) {
                this.setState({error:" - Please fill"})
                return
            }
            const booksEntry = Object.entries(this.props.booksList.list)
                .filter(([n,book])=>book!==undefined && book!==null) // filter undefined, any case
                .find(([n,{name}])=>name===this.bookNameInput.value)

            if(booksEntry) {
                this.setState({error:" - Book with that name exists"})
                return
            }

            this.closePopover()
            if(this.state.action==="new") this.props.addBook(this.bookNameInput.value, this.props, this.props.copyBook)
            else if(this.state.action==="rename") this.props.renameBook(this.bookNameInput.value, this.props)
        } else if(event.key === "Escape") {
            this.bookNameInput.value = ""
            window.getSelection().removeAllRanges()
            event.target.blur()
        }
    }
}

EditBooks.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default connect(
    state => ({
        booksList: state.booksList,
        backend: state.backend,
        bookData: state.bookData,
        bookLayout: state.bookLayout
    }),
    dispatch => ({
        selectBook: (name, state) => {
            const book = Object.entries(state.booksList.list).find(([,book])=>book && book.name===name)
            const bookIndex = book.length>0?book[0]:state.booksList.selectedBook
            dispatch({ // drop preview data
                type: "previewData",
                payload: {section: undefined,page: undefined,id: undefined}
            })
            dispatch({ // drop className to default
                type: "mainBoard",
                payload: {className: "odd"}
            })
            dispatch({
                type: "booksList",
                payload: {selectedBook: bookIndex}
            })
        },
        addBook: (name, state, copy) =>  {
            let taskIndex = state.backend.lastTaskIndex + 1
            const bookIndex = state.booksList.lastBookIndex===""
                ? 0
                : Number(state.booksList.lastBookIndex) + 1 // increase index of last section
            dispatch({                          // postMiddleware will not react on this
                type: "booksList.list."+bookIndex,
                payload: {name: name}
            })
            dispatch({                          // postMiddleware will not react on this
                type: "booksList.lastBookIndex",
                payload: bookIndex
            })
            dispatch({                          // postMiddleware will not react on this
                type: "booksList.selectedBook",
                payload: bookIndex
            }) // now backend will send data for book with new name
            dispatch({
                type: 'backend.tasks',
                payload: {
                    ...copy?{...state.bookData?Object.entries(state.bookData).reduce((acc,[section,pages])=>({ // copy data
                        ...acc,
                        ...pages?Object.entries(pages).reduce((acc2,[n,page])=>({
                            ...acc2,
                            ...page?Object.entries(page).reduce((acc3,[field,value])=>({
                                ...acc3,
                                [taskIndex++]: {["bookData."+section+"."+n+"."+field]: value?value:"",book: name}
                            }),{}):{}
                        }),{}):{}
                    }),{}):{},
                    ...Object.entries(state.bookLayout.sections).reduce((acc,[n,sectionLayout])=>({ // copy layout
                        ...acc,
                        ...sectionLayout?Object.entries(sectionLayout).reduce((acc2,[field,value])=>({
                            ...acc2,
                            [taskIndex++]: {["bookLayout.sections."+n+"."+field]: value, book: name},
                        }),{}):{}
                    }),{}),
                        [taskIndex++]: {"bookLayout.lastSectionIndex": state.bookLayout.lastSectionIndex, book: name},
                    }:{},
                    [taskIndex++]: {["booksList.list."+bookIndex+".name"]: name},
                    [taskIndex++]: {"booksList.lastBookIndex": bookIndex},
                    [taskIndex]: {"booksList._selectedBook": bookIndex}, // switch to added book
                }
            })
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
            dispatch({ // drop preview data
                type: "previewData",
                payload: {section: undefined,page: undefined,id: undefined}
            })
            dispatch({ // drop className to default
                type: "mainBoard",
                payload: {className: "odd"}
            })
        },
        renameBook: (newName, state) =>  {
            const bookIndex = state.booksList.selectedBook
            dispatch({                          // postMiddleware will not react on this
                type: "booksList.list."+bookIndex,
                payload: {name: newName}
            }) // now backend will send data for book with newName
            let taskIndex = state.backend.lastTaskIndex + 1
            dispatch({
                type: 'backend.tasks',
                payload: {
                    [taskIndex++]: {["booksList.list."+bookIndex+".name"]: newName,book: ""},
                    ...state.bookData?Object.entries(state.bookData).reduce((acc,[section,pages])=>({ // copy data
                        ...acc,
                        ...pages?Object.entries(pages).reduce((acc2,[n,page])=>({
                            ...acc2,
                            ...page?Object.entries(page).reduce((acc3,[field,value])=>({
                                ...acc3,
                                [taskIndex++]: {["bookData."+section+"."+n+"."+field]: value?value:"",book: newName}
                            }),{}):{}
                        }),{}):{}
                    }),{}):{},
                    ...Object.entries(state.bookLayout.sections).reduce((acc,[n,sectionLayout])=>({ // copy layout
                        ...acc,
                        ...sectionLayout?Object.entries(sectionLayout).reduce((acc2,[field,value])=>({
                            ...acc2,
                            [taskIndex++]: {["bookLayout.sections."+n+"."+field]: value, book: newName},
                        }),{}):{}
                    }),{}),
                    [taskIndex]: {"bookLayout.lastSectionIndex": state.bookLayout.lastSectionIndex, book: newName},
                }
            })
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
        },
        deleteBook: (name, state) =>  {
            const bookIndex = Number(state.booksList.selectedBook)
            let newBookIndex
            let taskIndex = state.backend.lastTaskIndex
            if(bookIndex === Number(state.booksList.lastBookIndex)) {
                newBookIndex = bookIndex-1
                dispatch({                                   // decrease book's index
                    type: "backend.tasks",
                    payload: {
                        [++taskIndex]: {
                            "booksList.lastBookIndex": (state.booksList.lastBookIndex-1)
                        }, // shift index
                    }
                })
            } else {                                             // shift UP all followed books
                newBookIndex = bookIndex // same book index, new name
                dispatch({                          // postMiddleware will not react on this
                    type: "booksList.list."+bookIndex+".name",
                    payload: state.booksList.list[bookIndex+1]
                }) // now backend will send data for book with newName
                const len = state.booksList.lastBookIndex - bookIndex
                dispatch({                                   // save shifted books in backend
                    type: "backend.tasks",
                    payload: {
                        [++taskIndex]: {  // #1 shift index
                            "booksList.lastBookIndex": (state.booksList.lastBookIndex-1)},
                        ...new Array(len).fill(undefined).reduce((acc,nothing,n)=>{
                            const bi = state.booksList.lastBookIndex-n   // #2 REVERSE order of changes
                            return {
                                ...acc,
                                [++taskIndex]: {
                                    ["booksList.list."+(bi-1)+".name"]: state.booksList.list[bi].name},
                            }
                        },{}),
                    }
                })
                dispatch({                                   // shift books local
                    type: "booksList.list",
                    payload: new Array(len).fill(undefined).reduce((acc,nothing,n)=>{
                            const bi = state.booksList.lastBookIndex-n   // #2 REVERSE order of changes
                            return {
                                ...acc,
                                ["booksList.list."+(bi-1)+".name"]: state.booksList.list[bi].name,
                            }
                        },{}),
                })
                dispatch({
                    type: "booksList",
                    payload:  {lastBookIndex: state.booksList.lastBookIndex-1}
                })
            }
            dispatch({ // fix an index of last task
                type: "backend.lastTaskIndex",
                payload: taskIndex
            })
            dispatch({ // drop preview data
                type: "previewData",
                payload: {section: undefined,page: undefined,id: undefined}
            })
            dispatch({ // drop className to default
                type: "mainBoard",
                payload: {className: "odd"}
            })
            dispatch({
                type: "booksList",
                payload: {selectedBook: newBookIndex}
            })
        },
    })
)(withStyles(styles)(EditBooks))