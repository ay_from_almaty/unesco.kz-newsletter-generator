/* eslint-disable flowtype/require-valid-file-annotation */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
// import Input, { InputLabel } from 'material-ui/Input';
// import { MenuItem } from 'material-ui/Menu';
// import { FormControl } from 'material-ui/Form';
// import Select from 'material-ui/Select';
import {connect} from 'react-redux'
// import Button from 'material-ui/Button';
import Popover from 'material-ui/Popover';
// import { findDOMNode } from 'react-dom';
// import TextField from 'material-ui/TextField';
import constants from '../../../constants'
import Typography from 'material-ui/Typography'
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Button from 'material-ui/Button';

const styles = theme => ({
    textField: {
        minWidth: 400,
    },
    container: {
        display: 'flex',
        flexWrap: "nowrap",
    },
    formControl: {
        marginTop: constants.theme.spacing.unit/4,
        display: 'flex',
        flexWrap:"wrap",
        width: "300px",
        marginRight: constants.theme.spacing.unit
    }
});

// dummy component, manage props.open outside, synchronize in onRequestClose()
const EditSectionPopover = (props) => <Popover className={"popover "+props.className}
                            open={props.open}
                            anchorEl={props.anchorEl}
                            onRequestClose={props.onRequestClose}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}>
        <div noValidate>
            <Typography style={{"textAlign":"center"}} type="title">
                <div className="dynamicColor">Layout settings for section "{props.sectionLayout.section}"</div>
            </Typography>
            <div>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                value="Hide header"
                                onChange={(event, checked) => props.onChange!==undefined
                                    && props.onChange({
                                        section: props.sectionLayout.section,
                                        hideHeader: checked?"true":""
                                    })}
                                checked={props.sectionLayout.hideHeader}
                            />
                        }
                        label="Hide header in the pages (header overlapping page content by default)"
                    />
                </FormGroup>
            </div>
            <div style={{textAlign:"right"}}>
                <Button raised onClick={(event)=>{
                    if(props.onChange) props.onChange({
                        section: props.sectionLayout.section,
                        hideHeader: props.sectionLayout.hideHeader?"true":""
                    },true)
                    if(props.onRequestClose) props.onRequestClose(event)
                }}
                        style={{marginRight: constants.theme.spacing.unit/2}}>
                    Apply to all sections
                </Button>
            </div>
        </div>
    </Popover>

EditSectionPopover.propTypes = {
    // outside:
    open: PropTypes.bool.isRequired,
    sectionLayout: PropTypes.object.isRequired,
    anchorEl: PropTypes.object.isRequired,
    onChange: PropTypes.func,   // new sectionLayout as parameter 1, flag for updating all sections as parameter2
    onRequestClose: PropTypes.func.isRequired,
    //
    classes: PropTypes.object.isRequired,
    className: PropTypes.string.isRequired
}

export default connect(
    state => ({
        className: state.mainBoard.className,
    }),
    dispatch => ({})
)(withStyles(styles)(EditSectionPopover))