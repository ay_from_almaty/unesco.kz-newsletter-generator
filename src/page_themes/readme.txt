Themes provides an SVG files for pages.
You can set the theme in /src/book/Page.css - just change a theme name in URLs

These files (just 3 files):
1. background/regular_yellow.svg - Background image for page from a section with odd number
2. background/regular_blue.svg - Background image for page from a section with even number
3. foreground/header.svg - Foreground image for header
