export default {
    pageLayout: ['header','title','content','footer'],
    sectionLayout:  ['section','pagesCount','hideHeader'],
    languages: ["English","Русский"],
    theme: {
        spacing: {unit: 25}
    },
    version: "19.10.17" // update manually at every release
}