<?php
/** An working example */

/** Developed for UNESCO Almaty Cluster Office */

/** 2017, a.yemelyanov@gmail.com, Backend for Newsletter App - ONE PHP SCRIPT - ONE SQL TABLE

CREATE TABLE `newsletter_dev` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`book` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT 'Newsletter in English' COMMENT 'Book''s name',
`name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'Name in format x.y.z',
`value` mediumtext CHARACTER SET utf8 NOT NULL COMMENT 'text value',
`stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'time of change',
`author` varchar(24) NOT NULL COMMENT 'author of change (authenticated via BASIC-AUTH)',
PRIMARY KEY (`id`),
KEY `name` (`name`),
KEY `book` (`book`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Backend for react newsletter app';

 */

function _die($message) {
    global $postdata;
    error_log("Newsletter backend: $message");
    error_log("input: ".$postdata);
    exit('{"error":"'.str_replace("\"","'",$message).'"}');
}

function clean_json($value) {
    $value = str_replace("\\", "\\\\", $value);
    $value = str_replace("\r", "", $value);
    $value = str_replace("\n", "", $value);
    $value = str_replace("\t", "", $value);
    $value = str_replace("\"", "\\\"", $value);
    //$value = mb_convert_encoding($value,"UTF-8","Windows-1251");
    return $value;
}

function _log($message) { /*error_log($message);*/ }

$table = $_GET["dev"]?"newsletter_dev":"newsletter";

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Credentials: true');
header('Content-type: application/json');

$postdata = file_get_contents("php://input");
if(!$postdata) _die("empty post data");

//_log("postdata: $postdata");
//_die("testing");

@mysql_connect("localhost", "root", "unescosql") or _die("Error (connect):".mysql_error ());
@mysql_select_db( "website") or _die("Error: (select DB) ".mysql_error ());

$as = explode("[{",$postdata); $as = explode("}]",$as[1]);
$as = explode("},{", $as[0]);

$books = array();

$sqlInsert = "";$sqlSelect = "";    $get=array();// last values will be here
// Send one query using good old features of mysql.
foreach($as as $hash) {
    $ohash = $hash;
    $hash = explode(",\"",$ohash);
    $name = $hash[0];
    $book = $hash[1][0]!='"'?"\"".$hash[1]:$hash[1]; // "book":"..."
    $hash = explode("\":\"",$book);
    $book = substr($hash[1],0, strlen($hash[1])-1); // strip "
    $hash = $name;                                    // task
    $ohash = $hash;

    $hash = explode("\":\"",$hash);
    if(!$hash[1]) $hash = explode("\":",$ohash);
    $name = substr($hash[0],1);
    $value = $hash[1][strlen($hash[1])-1]=='"'?substr($hash[1],0, strlen($hash[1])-1):$hash[1];

    if(!is_string($name)) // wrong parsing
        _die("Can't extract name from '$ohash''");

    $books[$name] = $book;

    if($value!=="null") { // null means "only get"
        if(!is_string($value)) // wrong parsing
            _die("Can't extract value($name) from $ohash");

        $sqlInsert .= "('$book','$name','".str_replace("'","\\'",$value)."','$_SERVER[REMOTE_USER]'),";
    }

    if(!$get[$name]) {
        $sqlSelect .= "(select value from $table where book='$book' AND name='$name' order by id desc limit 0,1) as '$name',";
        $get[$name] = 1; // mark to prevent adding
    }
}

if($sqlInsert) { // set: store as new rows to stay immutable
    $sqlInsert = substr($sqlInsert,0,strlen($sqlInsert)-1);
    _log("sqlInsert ".$sqlInsert);

    $r = @mysql_query("insert into $table(book,name,value,author) values".$sqlInsert);
    if(!$r) _die("Can't set values. ".mysql_error());
}

$json = "{";
if($sqlSelect) { // get last values
    $sqlSelect = substr($sqlSelect,0,strlen($sqlSelect)-1);
    _log("sqlSelect ".$sqlSelect);
    $r = @mysql_query("select ".$sqlSelect);

    if($r && ($row = mysql_fetch_array($r))) {
        foreach($row as $name=>$value) {
            if(!is_numeric($name)) {
                //$value = str_replace("\\", "\\\\", $value);
                $book = $books[$name];
                if(!$book) $book = "";
                $json .= '"' . $name . '":{"value":"' . clean_json($value) . '","book":"'.$book.'"},';
            }
        }
    } else _die("Newsletter backend: Can't get values. ".mysql_error());
}
$json = strlen($json)>1?substr($json,0,strlen($json)-1)."}":"{}";
//sleep(10);
_log($json);
echo $json;
