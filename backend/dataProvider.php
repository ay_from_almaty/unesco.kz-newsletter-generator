<?php
/** An working example */

/** Developed for UNESCO Almaty Cluster Office */

/** 2017, a.yemelyanov@gmail.com.  Provider of data for newsletter app
 *
 *  1. Extracts records from UNESCO Almaty database
 *  2. Create json
 *
 *  Structure: {month:{sector:[{id: ...}]}} - {id: ...} is article
 *
 *   Parameters passed via GET:
 *      from,to - date - Specifies the time interval for which articles are selected, date in format YYYY-MM-DD
 *      themes - 1 - Extracts list of themes from web-site menu and returns it as [{theme1},{theme2},...]
 *      theme - Specifies the theme for which an articles will be selected
 *      lang - 0/1 - English/Russian
 */

global $lang;

$lang = $_GET['lang'];

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Credentials: true');
header('Content-type: application/json; charset=utf-8');

include("../../../../en-ru.unesco.kz/connect.php"); // just connect to db
include("../../../../en-ru.unesco.kz/func/images.php"); // get_image(news_id) function that sends SQL to images table
include("../../../../en-ru.unesco.kz/ng_menu.php"); // to get site menu in $menu
global $menu;

if($_GET[themes]) { // Extracts list of themes from web-site menu and returns it as ["theme1","theme2",...]
    $list = list_root_submenu($menu,"Themes",$menu);
    if($list) exit($list);
    else _die("The root submenu named 'Themes' not found in the menu");
}

$theme_sql = "";

if($themes=$_GET[theme]) {
    foreach ($themes as $theme_name) {
        $theme_name = mb_convert_encoding($theme_name, "Windows-1251", "UTF-8");
        $theme = find_theme($menu, $theme_name);
        if ($theme) {
            $arr = explode("/", $theme);
            if ($arr[0] == "sector") $theme_sql .= " OR (sector='$theme_name' || sector_ru='$theme_name')";
            else if ($arr[0] == "tag") $theme_sql .= " OR MATCH(`title`,news.`description`,`fulltext_en`) AGAINST('" . tag_to_keywords($arr[1]) . "' in boolean mode)";
        }
    }
    if($theme_sql) $theme_sql = " AND (".substr($theme_sql,4).")"; // skip first OR
}

function find_theme ($menu,$theme_name) {
    foreach($menu as $root) {
        if(strpos($root[1],"themes")!==FALSE) {
            //var_dump($root[3]);
            $i = 0;
            foreach($root[3] as $sub)
                if(($i++)%2)
                    foreach($sub as $theme)
                        if($theme[1]==$theme_name) return($theme[0]);
        }
    }
    return null;
}

function list_root_submenu($menu,$title) {
    for($i=0;$i<count($menu);$i++) {
        if($menu[$i][0] == $title) {
            $json = "[";
            $rlen = count($menu[$i][3])/2;
            for($r=0;$r<$rlen;$r++) {
                $arr = $menu[$i][3][$r*2+1];
                $len = count($arr);
                for ($j = 0; $j < $len; $j++) {
                    $json .= "\"" . clean_json($arr[$j][1]) . "\",\r\n";
                }
            }
            if($json[strlen($json)-3]==",") $json = substr($json,0,strlen($json)-3);
            $json .= "]";
            return $json;
        }
    }
    return null;
}

function clean_json($value) {
    $value = str_replace("\\", "\\\\", $value);
    $value = str_replace("\r", "", $value);
    $value = str_replace("\n", "", $value);
    $value = str_replace("\t", "", $value);
    $value = str_replace("\"", "\\\"", $value);
    $value = mb_convert_encoding($value,"UTF-8","Windows-1251");
    return $value;
}

function _die($message) {
    global $postdata;
    error_log("Newsletter dataProvider: $message");
    error_log("input: ".$postdata);
    exit('{"error":"'.str_replace("\"","'",$message).'"}');
}

$to = $_GET[to]?$_GET[to]:date("Y-m-d");
$from = $_GET[from]?$_GET[from]:substr($to,0,3).($to[3]-1).substr($to,4);

error_log($to." ".$from);

$sql_fields = $lang
    ? "id,title_ru as title,description_ru as description,sector,region,link_ru as link,type_publication,fulltext_ru as `fulltext`,readable_url"
    : "id,title,description,sector,region,link,type_publication,fulltext_en as `fulltext`,readable_url";
$sql_fields.=",title as title_en,description as description_en,fulltext_en"; // always return English fields
$r = @mysql_query("SELECT $sql_fields,event_date FROM `news` WHERE
 intranet<>1 $theme_sql AND
 event_date <= '$to' AND event_date >= '$from'
  ORDER BY event_date DESC");

error_log($theme_sql);
if(!$r) _die(mysql_error());

$sectors = array();
$months = array();

$last_month = "";

$month_names = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

while($row = mysql_fetch_array($r)) {
    $dd = explode("-",$row[event_date]);
    $lm = $month_names[$dd[1]-1]." ".$dd[0];
    if($last_month=="") {
        $last_month = $lm;
    } else if($last_month!=$lm) {
        $months[$last_month] = $sectors;
        $sectors = array();
        $last_month = $lm;
    }
    $_row = array();
    foreach ($row as $name=>$field) {
        if(!is_numeric($name)) $_row[$name] = $field;
    }
    if(!$sectors[$row[sector]]) $sectors[$row[sector]] = array();
    $_row[image] = get_image($row[id]);
    array_push($sectors[$row[sector]],$_row);
}
$months[$lm] = $sectors;

//$value = str_replace("\\", "\\\\", $value);
//$json .= '"' . $name . '":"' . str_replace("\"", "\\\"", $value) . '",';

//print_r($months);

$s = "";

foreach($months as $month => $sectors) {
    $s .= "\"$month\": {\r\n";
    foreach($sectors as $sector => $articles) {
        $s .= "\t\"$sector\": [\r\n";
        foreach($articles as $article) {
            $s .= "\t\t{\r\n";
            foreach($article as $name => $field) {
                if($name=="image") {
                    if($field) {
                        $s .= "\t\t\t\"image\": {\r\n";
                        foreach ($field as $_name => $_field) {
                            if (!is_numeric($_name)) {
                                $_value = str_replace("\\", "\\\\", $_field);
                                $s .= "\t\t\t\t\"$_name\":\"" . str_replace("\"", "\\\"", $_value) . "\",\r\n";
                            }
                        };

                        if ($s[strlen($s) - 3] == ",") $s = substr($s, 0, strlen($s) - 3) . "\r\n";
                        $s .= "\t\t\t},\r\n";
                    }
                } else if($name=="fulltext") {
                    if($field) $s .= "\t\t\t\"$name\":\"".clean_json(add_images(preg_replace("/%\d+%/", "", $field)))."\",\r\n";
                } else {
                    $s .= "\t\t\t\"$name\":\"".clean_json($field)."\",\r\n";
                }
            };
            if($s[strlen($s)-3]==",") $s = substr($s,0,strlen($s)-3)."\r\n";
            $s .= "\t\t},\r\n";
        };
        if($s[strlen($s)-3]==",") $s = substr($s,0,strlen($s)-3)."\r\n";
        $s .= "\t],\r\n";
    };
    if($s[strlen($s)-3]==",") $s = substr($s,0,strlen($s)-3)."\r\n";
    $s .= "},\r\n";
}
if($s[strlen($s)-3]==",") $s = substr($s,0,strlen($s)-3)."\r\n";
echo "{".$s."}";
